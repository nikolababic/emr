# Diplomski rad

Platforma za pružanje pomoći u zdravstvenim uslugama.

## Development

Potrebno je instalirati Node.js

Nakon toga u folderu gde se nalazi i [package.json](package.json) treba izvršiti:
```
npm install
```

Za praćenje izmena na fajlovima i njihovo prikazivanje u web pregledaču potrebno je izvršiti sledeće dve komande.

```
./gradlew -x webapp
npm start
```

Java projekat se može pokrenuti i kroz eclipse.
