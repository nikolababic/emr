package rs.emr.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import rs.emr.domain.Participants;

/**
 * Service Interface for managing {@link Participants}.
 */
public interface ParticipantsService {
    /**
     * Save a participants.
     *
     * @param participants the entity to save.
     * @return the persisted entity.
     */
    Participants save(Participants participants);

    /**
     * Partially updates a participants.
     *
     * @param participants the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Participants> partialUpdate(Participants participants);

    /**
     * Get all the participants.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Participants> findAll(Pageable pageable);

    /**
     * Get the "id" participants.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Participants> findOne(Long id);

    /**
     * Delete the "id" participants.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
