package rs.emr.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import rs.emr.domain.WorkHour;

/**
 * Service Interface for managing {@link WorkHour}.
 */
public interface WorkHourService {
    /**
     * Save a workHour.
     *
     * @param workHour the entity to save.
     * @return the persisted entity.
     */
    WorkHour save(WorkHour workHour);

    /**
     * Partially updates a workHour.
     *
     * @param workHour the entity to update partially.
     * @return the persisted entity.
     */
    Optional<WorkHour> partialUpdate(WorkHour workHour);

    /**
     * Get all the workHours.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<WorkHour> findAll(Pageable pageable);

    /**
     * Get the "id" workHour.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<WorkHour> findOne(Long id);

    /**
     * Delete the "id" workHour.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
