package rs.emr.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import rs.emr.domain.Conversation;

/**
 * Service Interface for managing {@link Conversation}.
 */
public interface ConversationService {
    /**
     * Save a conversation.
     *
     * @param conversation the entity to save.
     * @return the persisted entity.
     */
    Conversation save(Conversation conversation);

    /**
     * Partially updates a conversation.
     *
     * @param conversation the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Conversation> partialUpdate(Conversation conversation);

    /**
     * Get all the conversations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Conversation> findAll(Pageable pageable);

    /**
     * Get the "id" conversation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Conversation> findOne(Long id);

    /**
     * Delete the "id" conversation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
	List<Map<String, ?>> getMyConversations(Long userId);
}
