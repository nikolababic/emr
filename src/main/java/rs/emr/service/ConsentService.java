package rs.emr.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import rs.emr.domain.Consent;

/**
 * Service Interface for managing {@link Consent}.
 */
public interface ConsentService {
    /**
     * Save a consent.
     *
     * @param consent the entity to save.
     * @return the persisted entity.
     */
    Consent save(Consent consent);

    /**
     * Partially updates a consent.
     *
     * @param consent the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Consent> partialUpdate(Consent consent);

    /**
     * Get all the consents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Consent> findAll(Pageable pageable);

    /**
     * Get the "id" consent.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Consent> findOne(Long id);
    
    List<Consent> findForDoctor();
    
    List<Map<String, ?>> findAllForDoctor();

    /**
     * Delete the "id" consent.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
