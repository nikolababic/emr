package rs.emr.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import rs.emr.domain.RecordType;

/**
 * Service Interface for managing {@link RecordType}.
 */
public interface RecordTypeService {
    /**
     * Save a recordType.
     *
     * @param recordType the entity to save.
     * @return the persisted entity.
     */
    RecordType save(RecordType recordType);

    /**
     * Partially updates a recordType.
     *
     * @param recordType the entity to update partially.
     * @return the persisted entity.
     */
    Optional<RecordType> partialUpdate(RecordType recordType);

    /**
     * Get all the recordTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RecordType> findAll(Pageable pageable);

    /**
     * Get the "id" recordType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RecordType> findOne(Long id);

    /**
     * Delete the "id" recordType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
