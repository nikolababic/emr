package rs.emr.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link rs.emr.domain.Document} entity. This class is used
 * in {@link rs.emr.web.rest.DocumentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /documents?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DocumentCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter file;

    private InstantFilter documentDate;

    private StringFilter note;

    private LongFilter userId;

    private LongFilter recordId;

    private Boolean distinct;

    public DocumentCriteria() {}

    public DocumentCriteria(DocumentCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.file = other.file == null ? null : other.file.copy();
        this.documentDate = other.documentDate == null ? null : other.documentDate.copy();
        this.note = other.note == null ? null : other.note.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.recordId = other.recordId == null ? null : other.recordId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public DocumentCriteria copy() {
        return new DocumentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getFile() {
        return file;
    }

    public StringFilter file() {
        if (file == null) {
            file = new StringFilter();
        }
        return file;
    }

    public void setFile(StringFilter file) {
        this.file = file;
    }

    public InstantFilter getDocumentDate() {
        return documentDate;
    }

    public InstantFilter documentDate() {
        if (documentDate == null) {
            documentDate = new InstantFilter();
        }
        return documentDate;
    }

    public void setDocumentDate(InstantFilter documentDate) {
        this.documentDate = documentDate;
    }

    public StringFilter getNote() {
        return note;
    }

    public StringFilter note() {
        if (note == null) {
            note = new StringFilter();
        }
        return note;
    }

    public void setNote(StringFilter note) {
        this.note = note;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public LongFilter userId() {
        if (userId == null) {
            userId = new LongFilter();
        }
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getRecordId() {
        return recordId;
    }

    public LongFilter recordId() {
        if (recordId == null) {
            recordId = new LongFilter();
        }
        return recordId;
    }

    public void setRecordId(LongFilter recordId) {
        this.recordId = recordId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DocumentCriteria that = (DocumentCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(file, that.file) &&
            Objects.equals(documentDate, that.documentDate) &&
            Objects.equals(note, that.note) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(recordId, that.recordId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, file, documentDate, note, userId, recordId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DocumentCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (file != null ? "file=" + file + ", " : "") +
            (documentDate != null ? "documentDate=" + documentDate + ", " : "") +
            (note != null ? "note=" + note + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            (recordId != null ? "recordId=" + recordId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
