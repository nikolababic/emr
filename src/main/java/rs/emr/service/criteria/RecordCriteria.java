package rs.emr.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link rs.emr.domain.Record} entity. This class is used
 * in {@link rs.emr.web.rest.RecordResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /records?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RecordCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private InstantFilter recordDate;

    private StringFilter additionalInformation;

    private BooleanFilter active;

    private LongFilter userId;

    private LongFilter recordTypeId;

    private Boolean distinct;

    public RecordCriteria() {}

    public RecordCriteria(RecordCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.recordDate = other.recordDate == null ? null : other.recordDate.copy();
        this.additionalInformation = other.additionalInformation == null ? null : other.additionalInformation.copy();
        this.active = other.active == null ? null : other.active.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.recordTypeId = other.recordTypeId == null ? null : other.recordTypeId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public RecordCriteria copy() {
        return new RecordCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public InstantFilter getRecordDate() {
        return recordDate;
    }

    public InstantFilter recordDate() {
        if (recordDate == null) {
            recordDate = new InstantFilter();
        }
        return recordDate;
    }

    public void setRecordDate(InstantFilter recordDate) {
        this.recordDate = recordDate;
    }

    public StringFilter getAdditionalInformation() {
        return additionalInformation;
    }

    public StringFilter additionalInformation() {
        if (additionalInformation == null) {
            additionalInformation = new StringFilter();
        }
        return additionalInformation;
    }

    public void setAdditionalInformation(StringFilter additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public BooleanFilter getActive() {
        return active;
    }

    public BooleanFilter active() {
        if (active == null) {
            active = new BooleanFilter();
        }
        return active;
    }

    public void setActive(BooleanFilter active) {
        this.active = active;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public LongFilter userId() {
        if (userId == null) {
            userId = new LongFilter();
        }
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getRecordTypeId() {
        return recordTypeId;
    }

    public LongFilter recordTypeId() {
        if (recordTypeId == null) {
            recordTypeId = new LongFilter();
        }
        return recordTypeId;
    }

    public void setRecordTypeId(LongFilter recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RecordCriteria that = (RecordCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(recordDate, that.recordDate) &&
            Objects.equals(additionalInformation, that.additionalInformation) &&
            Objects.equals(active, that.active) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(recordTypeId, that.recordTypeId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, recordDate, additionalInformation, active, userId, recordTypeId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RecordCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (recordDate != null ? "recordDate=" + recordDate + ", " : "") +
            (additionalInformation != null ? "additionalInformation=" + additionalInformation + ", " : "") +
            (active != null ? "active=" + active + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            (recordTypeId != null ? "recordTypeId=" + recordTypeId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
