package rs.emr.service.impl;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.emr.domain.Consent;
import rs.emr.repository.ConsentRepository;
import rs.emr.service.ConsentService;

/**
 * Service Implementation for managing {@link Consent}.
 */
@Service
@Transactional
public class ConsentServiceImpl implements ConsentService {

    private final Logger log = LoggerFactory.getLogger(ConsentServiceImpl.class);

    private final ConsentRepository consentRepository;
    
    public ConsentServiceImpl(ConsentRepository consentRepository) {
        this.consentRepository = consentRepository;
    }

    @Override
    public Consent save(Consent consent) {
        log.debug("Request to save Consent : {}", consent);
        return consentRepository.save(consent);
    }

    @Override
    public Optional<Consent> partialUpdate(Consent consent) {
        log.debug("Request to partially update Consent : {}", consent);

        return consentRepository
            .findById(consent.getId())
            .map(existingConsent -> {
                if (consent.getConsentStatus() != null) {
                    existingConsent.setConsentStatus(consent.getConsentStatus());
                }

                return existingConsent;
            })
            .map(consentRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Consent> findAll(Pageable pageable) {
        log.debug("Request to get all Consents");
        return consentRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Consent> findOne(Long id) {
        log.debug("Request to get Consent : {}", id);
        return consentRepository.findById(id);
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Consent> findForDoctor() {
        return consentRepository.findByDoctorIsCurrentUser();
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Consent : {}", id);
        consentRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<Map<String, ?>> findAllForDoctor() {
    	return consentRepository.findAllForDoctor();
    }
}
