package rs.emr.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.emr.domain.WorkHour;
import rs.emr.repository.WorkHourRepository;
import rs.emr.service.WorkHourService;

/**
 * Service Implementation for managing {@link WorkHour}.
 */
@Service
@Transactional
public class WorkHourServiceImpl implements WorkHourService {

    private final Logger log = LoggerFactory.getLogger(WorkHourServiceImpl.class);

    private final WorkHourRepository workHourRepository;

    public WorkHourServiceImpl(WorkHourRepository workHourRepository) {
        this.workHourRepository = workHourRepository;
    }

    @Override
    public WorkHour save(WorkHour workHour) {
        log.debug("Request to save WorkHour : {}", workHour);
        return workHourRepository.save(workHour);
    }

    @Override
    public Optional<WorkHour> partialUpdate(WorkHour workHour) {
        log.debug("Request to partially update WorkHour : {}", workHour);

        return workHourRepository
            .findById(workHour.getId())
            .map(existingWorkHour -> {
                if (workHour.getTimeFrom() != null) {
                    existingWorkHour.setTimeFrom(workHour.getTimeFrom());
                }
                if (workHour.getTimeTo() != null) {
                    existingWorkHour.setTimeTo(workHour.getTimeTo());
                }

                return existingWorkHour;
            })
            .map(workHourRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<WorkHour> findAll(Pageable pageable) {
        log.debug("Request to get all WorkHours");
        return workHourRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<WorkHour> findOne(Long id) {
        log.debug("Request to get WorkHour : {}", id);
        return workHourRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete WorkHour : {}", id);
        workHourRepository.deleteById(id);
    }
}
