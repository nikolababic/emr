package rs.emr.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.emr.domain.Participants;
import rs.emr.repository.ParticipantsRepository;
import rs.emr.service.ParticipantsService;

/**
 * Service Implementation for managing {@link Participants}.
 */
@Service
@Transactional
public class ParticipantsServiceImpl implements ParticipantsService {

    private final Logger log = LoggerFactory.getLogger(ParticipantsServiceImpl.class);

    private final ParticipantsRepository participantsRepository;

    public ParticipantsServiceImpl(ParticipantsRepository participantsRepository) {
        this.participantsRepository = participantsRepository;
    }

    @Override
    public Participants save(Participants participants) {
        log.debug("Request to save Participants : {}", participants);
        return participantsRepository.save(participants);
    }

    @Override
    public Optional<Participants> partialUpdate(Participants participants) {
        log.debug("Request to partially update Participants : {}", participants);

        return participantsRepository
            .findById(participants.getId())
            .map(existingParticipants -> {
                if (participants.getSeen() != null) {
                    existingParticipants.setSeen(participants.getSeen());
                }

                return existingParticipants;
            })
            .map(participantsRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Participants> findAll(Pageable pageable) {
        log.debug("Request to get all Participants");
        return participantsRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Participants> findOne(Long id) {
        log.debug("Request to get Participants : {}", id);
        return participantsRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Participants : {}", id);
        participantsRepository.deleteById(id);
    }
}
