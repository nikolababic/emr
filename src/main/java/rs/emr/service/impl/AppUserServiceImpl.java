package rs.emr.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.emr.domain.AppUser;
import rs.emr.repository.AppUserRepository;
import rs.emr.service.AppUserService;

/**
 * Service Implementation for managing {@link AppUser}.
 */
@Service
@Transactional
public class AppUserServiceImpl implements AppUserService {

    private final Logger log = LoggerFactory.getLogger(AppUserServiceImpl.class);

    private final AppUserRepository appUserRepository;

    public AppUserServiceImpl(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public AppUser save(AppUser appUser) {
        log.debug("Request to save AppUser : {}", appUser);
        return appUserRepository.save(appUser);
    }

    @Override
    public Optional<AppUser> partialUpdate(AppUser appUser) {
        log.debug("Request to partially update AppUser : {}", appUser);

        return appUserRepository
            .findById(appUser.getId())
            .map(existingAppUser -> {
                if (appUser.getPhoneNumber() != null) {
                    existingAppUser.setPhoneNumber(appUser.getPhoneNumber());
                }
                if (appUser.getDateOfBirth() != null) {
                    existingAppUser.setDateOfBirth(appUser.getDateOfBirth());
                }
                if (appUser.getGender() != null) {
                    existingAppUser.setGender(appUser.getGender());
                }
                if (appUser.getTitle() != null) {
                    existingAppUser.setTitle(appUser.getTitle());
                }
                if (appUser.getSpecialization() != null) {
                    existingAppUser.setSpecialization(appUser.getSpecialization());
                }
                if (appUser.getMoney() != null) {
                    existingAppUser.setMoney(appUser.getMoney());
                }
                if (appUser.getNotes() != null) {
                    existingAppUser.setNotes(appUser.getNotes());
                }

                return existingAppUser;
            })
            .map(appUserRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AppUser> findAll(Pageable pageable) {
        log.debug("Request to get all AppUsers");
        return appUserRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AppUser> findOne(Long id) {
        log.debug("Request to get AppUser : {}", id);
        return appUserRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete AppUser : {}", id);
        appUserRepository.deleteById(id);
    }

    @Override
    public List<String> getSpecializations() {
    	return appUserRepository.getSpecializations();
    }
    
    @Override
    public List<Map<String, ?>> searchDoctors() {
    	return appUserRepository.searchDoctors();
    }
    
    @Override
    public void updateMoney(Long id, Double money) {
    	appUserRepository.updateMoney(id, money);
    }
}
