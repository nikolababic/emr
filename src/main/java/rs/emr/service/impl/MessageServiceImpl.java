package rs.emr.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.emr.domain.Message;
import rs.emr.repository.MessageRepository;
import rs.emr.repository.ParticipantsRepository;
import rs.emr.service.MessageService;

/**
 * Service Implementation for managing {@link Message}.
 */
@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    private final Logger log = LoggerFactory.getLogger(MessageServiceImpl.class);

    private final MessageRepository messageRepository;
    private final ParticipantsRepository participantsRepository;

    public MessageServiceImpl(MessageRepository messageRepository,  ParticipantsRepository participantsRepository) {
        this.messageRepository = messageRepository;
        this.participantsRepository = participantsRepository;
    }

    @Override
    public Message save(Message message) {
        log.debug("Request to save Message : {}", message);
        // @Update where user_id != user id
//        participantsRepository.
        return messageRepository.save(message);
    }

    @Override
    public Optional<Message> partialUpdate(Message message) {
        log.debug("Request to partially update Message : {}", message);

        return messageRepository
            .findById(message.getId())
            .map(existingMessage -> {
                if (message.getMessageText() != null) {
                    existingMessage.setMessageText(message.getMessageText());
                }
                if (message.getMessageDate() != null) {
                    existingMessage.setMessageDate(message.getMessageDate());
                }

                return existingMessage;
            })
            .map(messageRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Message> findAll(Pageable pageable) {
        log.debug("Request to get all Messages");
        return messageRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Message> findOne(Long id) {
        log.debug("Request to get Message : {}", id);
        return messageRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Message : {}", id);
        messageRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
	public List<Map<String, ?>> listMessagesForConvesation(Long convesationId) {
    	return messageRepository.listMessagesForConvesation(convesationId);
    }
}
