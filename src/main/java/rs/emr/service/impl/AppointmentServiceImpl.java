package rs.emr.service.impl;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.emr.domain.Appointment;
import rs.emr.domain.Consent;
import rs.emr.repository.AppointmentRepository;
import rs.emr.service.AppointmentService;

/**
 * Service Implementation for managing {@link Appointment}.
 */
@Service
@Transactional
public class AppointmentServiceImpl implements AppointmentService {

    private final Logger log = LoggerFactory.getLogger(AppointmentServiceImpl.class);

    private final AppointmentRepository appointmentRepository;

    public AppointmentServiceImpl(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }

    @Override
    public Appointment save(Appointment appointment) {
        log.debug("Request to save Appointment : {}", appointment);
        return appointmentRepository.save(appointment);
    }

    @Override
    public Optional<Appointment> partialUpdate(Appointment appointment) {
        log.debug("Request to partially update Appointment : {}", appointment);

        return appointmentRepository
            .findById(appointment.getId())
            .map(existingAppointment -> {
                if (appointment.getAppointmentDate() != null) {
                    existingAppointment.setAppointmentDate(appointment.getAppointmentDate());
                }
                if (appointment.getTimeFrom() != null) {
                    existingAppointment.setTimeFrom(appointment.getTimeFrom());
                }
                if (appointment.getTimeTo() != null) {
                    existingAppointment.setTimeTo(appointment.getTimeTo());
                }
                if (appointment.getAppointmentStatus() != null) {
                    existingAppointment.setAppointmentStatus(appointment.getAppointmentStatus());
                }

                return existingAppointment;
            })
            .map(appointmentRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Appointment> findAll(Pageable pageable) {
        log.debug("Request to get all Appointments");
        return appointmentRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Appointment> findOne(Long id) {
        log.debug("Request to get Appointment : {}", id);
        return appointmentRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Appointment : {}", id);
        appointmentRepository.deleteById(id);
    }
    

    @Override
    @Transactional(readOnly = true)
    public List<Appointment> findForDoctor() {
        return appointmentRepository.findByDoctorIsCurrentUser();
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Appointment> findForPatient() {
        return appointmentRepository.findByPatientIsCurrentUser();
    }
}
