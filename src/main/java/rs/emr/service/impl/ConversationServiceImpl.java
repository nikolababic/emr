package rs.emr.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.emr.domain.Conversation;
import rs.emr.repository.ConversationRepository;
import rs.emr.service.ConversationService;

/**
 * Service Implementation for managing {@link Conversation}.
 */
@Service
@Transactional
public class ConversationServiceImpl implements ConversationService {

    private final Logger log = LoggerFactory.getLogger(ConversationServiceImpl.class);

    private final ConversationRepository conversationRepository;

    public ConversationServiceImpl(ConversationRepository conversationRepository) {
        this.conversationRepository = conversationRepository;
    }

    @Override
    public Conversation save(Conversation conversation) {
        log.debug("Request to save Conversation : {}", conversation);
        return conversationRepository.save(conversation);
    }

    @Override
    public Optional<Conversation> partialUpdate(Conversation conversation) {
        log.debug("Request to partially update Conversation : {}", conversation);

        return conversationRepository
            .findById(conversation.getId())
            .map(existingConversation -> {
                if (conversation.getName() != null) {
                    existingConversation.setName(conversation.getName());
                }

                return existingConversation;
            })
            .map(conversationRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Conversation> findAll(Pageable pageable) {
        log.debug("Request to get all Conversations");
        return conversationRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Conversation> findOne(Long id) {
        log.debug("Request to get Conversation : {}", id);
        return conversationRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Conversation : {}", id);
        conversationRepository.deleteById(id);
    }
    
    @Transactional(readOnly = true)
	public List<Map<String, ?>> getMyConversations(Long userId) {
    	return conversationRepository.getMyConversations(userId);
    }
}
