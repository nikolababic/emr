package rs.emr.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.emr.domain.RecordType;
import rs.emr.repository.RecordTypeRepository;
import rs.emr.service.RecordTypeService;

/**
 * Service Implementation for managing {@link RecordType}.
 */
@Service
@Transactional
public class RecordTypeServiceImpl implements RecordTypeService {

    private final Logger log = LoggerFactory.getLogger(RecordTypeServiceImpl.class);

    private final RecordTypeRepository recordTypeRepository;

    public RecordTypeServiceImpl(RecordTypeRepository recordTypeRepository) {
        this.recordTypeRepository = recordTypeRepository;
    }

    @Override
    public RecordType save(RecordType recordType) {
        log.debug("Request to save RecordType : {}", recordType);
        return recordTypeRepository.save(recordType);
    }

    @Override
    public Optional<RecordType> partialUpdate(RecordType recordType) {
        log.debug("Request to partially update RecordType : {}", recordType);

        return recordTypeRepository
            .findById(recordType.getId())
            .map(existingRecordType -> {
                if (recordType.getName() != null) {
                    existingRecordType.setName(recordType.getName());
                }
                if (recordType.getVisibleColumns() != null) {
                    existingRecordType.setVisibleColumns(recordType.getVisibleColumns());
                }

                return existingRecordType;
            })
            .map(recordTypeRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RecordType> findAll(Pageable pageable) {
        log.debug("Request to get all RecordTypes");
        return recordTypeRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RecordType> findOne(Long id) {
        log.debug("Request to get RecordType : {}", id);
        return recordTypeRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RecordType : {}", id);
        recordTypeRepository.deleteById(id);
    }
}
