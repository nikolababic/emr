package rs.emr.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.emr.domain.*; // for static metamodels
import rs.emr.domain.Record;
import rs.emr.repository.RecordRepository;
import rs.emr.service.criteria.RecordCriteria;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Record} entities in the database.
 * The main input is a {@link RecordCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Record} or a {@link Page} of {@link Record} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecordQueryService extends QueryService<Record> {

    private final Logger log = LoggerFactory.getLogger(RecordQueryService.class);

    private final RecordRepository recordRepository;

    public RecordQueryService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    /**
     * Return a {@link List} of {@link Record} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Record> findByCriteria(RecordCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Record> specification = createSpecification(criteria);
        return recordRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Record} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Record> findByCriteria(RecordCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Record> specification = createSpecification(criteria);
        return recordRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecordCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Record> specification = createSpecification(criteria);
        return recordRepository.count(specification);
    }

    /**
     * Function to convert {@link RecordCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Record> createSpecification(RecordCriteria criteria) {
        Specification<Record> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Record_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Record_.name));
            }
            if (criteria.getRecordDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRecordDate(), Record_.recordDate));
            }
            if (criteria.getAdditionalInformation() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getAdditionalInformation(), Record_.additionalInformation));
            }
            if (criteria.getActive() != null) {
                specification = specification.and(buildSpecification(criteria.getActive(), Record_.active));
            }
            if (criteria.getUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getUserId(), root -> root.join(Record_.user, JoinType.LEFT).get(User_.id))
                    );
            }
            if (criteria.getRecordTypeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getRecordTypeId(),
                            root -> root.join(Record_.recordType, JoinType.LEFT).get(RecordType_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
