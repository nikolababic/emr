package rs.emr.web.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import rs.emr.domain.Document;
import rs.emr.domain.FormWrapper;
import rs.emr.domain.Record;
import rs.emr.domain.User;
import rs.emr.repository.DocumentRepository;
import rs.emr.service.DocumentQueryService;
import rs.emr.service.DocumentService;
import rs.emr.service.criteria.DocumentCriteria;
import rs.emr.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link rs.emr.domain.Document}.
 */
@RestController
@RequestMapping("/api")
public class DocumentResource {

	private final Logger log = LoggerFactory.getLogger(DocumentResource.class);

	private static final String ENTITY_NAME = "document";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final DocumentService documentService;

	private final DocumentRepository documentRepository;

	private final DocumentQueryService documentQueryService;

	public static String contextPath = File.separator;

	public DocumentResource(DocumentService documentService, DocumentRepository documentRepository,
			DocumentQueryService documentQueryService) {
		this.documentService = documentService;
		this.documentRepository = documentRepository;
		this.documentQueryService = documentQueryService;
	}

	/**
	 * {@code POST  /documents} : Create a new document.
	 *
	 * @param document the document to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new document, or with status {@code 400 (Bad Request)} if
	 *         the document has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/documents")
	public ResponseEntity<Document> createDocument(@Valid @RequestBody Document document) throws URISyntaxException {
		log.debug("REST request to save Document : {}", document);
		if (document.getId() != null) {
			throw new BadRequestAlertException("A new document cannot already have an ID", ENTITY_NAME, "idexists");
		}
		Document result = documentService.save(document);
		return ResponseEntity
				.created(new URI("/api/documents/" + result.getId())).headers(HeaderUtil
						.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	/**
	 * {@code PUT  /documents/:id} : Updates an existing document.
	 *
	 * @param id       the id of the document to save.
	 * @param document the document to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated document, or with status {@code 400 (Bad Request)} if the
	 *         document is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the document couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/documents/{id}")
	public ResponseEntity<Document> updateDocument(@PathVariable(value = "id", required = false) final Long id,
			@Valid @RequestBody Document document) throws URISyntaxException {
		log.debug("REST request to update Document : {}, {}", id, document);
		if (document.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		if (!Objects.equals(id, document.getId())) {
			throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
		}

		if (!documentRepository.existsById(id)) {
			throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
		}

		Document result = documentService.save(document);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, document.getId().toString()))
				.body(result);
	}

	/**
	 * {@code PATCH  /documents/:id} : Partial updates given fields of an existing
	 * document, field will ignore if it is null
	 *
	 * @param id       the id of the document to save.
	 * @param document the document to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated document, or with status {@code 400 (Bad Request)} if the
	 *         document is not valid, or with status {@code 404 (Not Found)} if the
	 *         document is not found, or with status
	 *         {@code 500 (Internal Server Error)} if the document couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PatchMapping(value = "/documents/{id}", consumes = { "application/json", "application/merge-patch+json" })
	public ResponseEntity<Document> partialUpdateDocument(@PathVariable(value = "id", required = false) final Long id,
			@NotNull @RequestBody Document document) throws URISyntaxException {
		log.debug("REST request to partial update Document partially : {}, {}", id, document);
		if (document.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		if (!Objects.equals(id, document.getId())) {
			throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
		}

		if (!documentRepository.existsById(id)) {
			throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
		}

		Optional<Document> result = documentService.partialUpdate(document);

		return ResponseUtil.wrapOrNotFound(result,
				HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, document.getId().toString()));
	}

	/**
	 * {@code GET  /documents} : get all the documents.
	 *
	 * @param pageable the pagination information.
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of documents in body.
	 */
	@GetMapping("/documents")
	public ResponseEntity<List<Document>> getAllDocuments(DocumentCriteria criteria, Pageable pageable) {
		log.debug("REST request to get Documents by criteria: {}", criteria);
		Page<Document> page = documentQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

	/**
	 * {@code GET  /documents/count} : count all the documents.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/documents/count")
	public ResponseEntity<Long> countDocuments(DocumentCriteria criteria) {
		log.debug("REST request to count Documents by criteria: {}", criteria);
		return ResponseEntity.ok().body(documentQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /documents/:id} : get the "id" document.
	 *
	 * @param id the id of the document to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the document, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/documents/{id}")
	public ResponseEntity<Document> getDocument(@PathVariable Long id) {
		log.debug("REST request to get Document : {}", id);
		Optional<Document> document = documentService.findOne(id);
		return ResponseUtil.wrapOrNotFound(document);
	}

	/**
	 * {@code DELETE  /documents/:id} : delete the "id" document.
	 *
	 * @param id the id of the document to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/documents/{id}")
	public ResponseEntity<Void> deleteDocument(@PathVariable Long id) {
		log.debug("REST request to delete Document : {}", id);
		documentService.delete(id);
		return ResponseEntity.noContent()
				.headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
				.build();
	}

	@PostMapping("/documents-add")
	public ResponseEntity<Document> addDocument(@ModelAttribute FormWrapper model)
			throws URISyntaxException {

		Document document = new Document();
		document.setName(model.getName());
		document.setDocumentDate(model.getDocumentDate());
		document.setNote(model.getNote());
		if (model.getRecordId() != null) {
			Record record = new Record();
			record.setId(Long.parseLong(model.getRecordId()));
			document.setRecord(record);
		}
		if (model.getUserId() != null) {
			User  user = new User();
			user.setId(Long.parseLong(model.getUserId()));
			document.setUser(user);
		}
		try {
			String file = storeDocument(model.getFile(), document);
			document.setFile(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document result = documentService.save(document);
		return ResponseEntity
				.created(new URI("/api/documents/" + result.getId())).headers(HeaderUtil
						.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	public String storeDocument(MultipartFile file, Document document) throws IOException {
		int count = 0;
		if (!file.isEmpty()) {
			// kreiranje foldera
			if (!(new File(getRootPath() + File.separator + "uploads" + File.separator).exists())) {
				new File(getRootPath() + File.separator + "uploads" + File.separator).mkdir();
			}
			if (!(new File(getRootPath() + File.separator + "uploads/documents" + File.separator).exists())) {
				new File(getRootPath() + File.separator + "uploads/documents" + File.separator).mkdir();
			}
			String rootDirectory = getRootPath() + File.separator + "uploads" + File.separator + "documents"
					+ File.separator + "user_" + document.getUser().getId() + File.separator;
			File directory = new File(rootDirectory);
			if (!directory.exists())
				directory.mkdir();
			if (new File(rootDirectory).listFiles() != null && new File(rootDirectory).listFiles().length != 0) {
				count = new File(rootDirectory).listFiles().length;
			}
			File newFile = this.convert(file);
			String[] split = file.getOriginalFilename().split("\\.");
			int i = split.length;
			String extension = split[split.length - 1];
			File uploadFile = new File(rootDirectory + File.separator + count + "." + extension);
			newFile.renameTo(uploadFile);
			return uploadFile.getName();
		}
		return null;
	}

	public File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	public static boolean isRunningUnderServletContainer() {
		try {
			new InitialContext().lookup("java:comp/env");
			return true;
		} catch (NamingException ex) {
			return false;
		}
	}

	public String getRootPath() {
		if (isRunningUnderServletContainer()) {
			String scRoot = new File("").getAbsolutePath();
			return new File(scRoot.replaceFirst("bin$|bin\\\\$", "") + getContextPath()).getAbsolutePath();
		} else
			return new File("").getAbsolutePath();
	}

	private String getContextPath() {
		String scRoot = new File("").getAbsolutePath();
		if (isRunningUnderServletContainer()) {
			return File.separator + "webapps" + File.separator + "ticket";
		} else {
			return (scRoot.matches("bin$|bin\\\\$") ? "/../" : "/./") + contextPath;
		}
	}
}
