/**
 * View Models used by Spring MVC REST controllers.
 */
package rs.emr.web.rest.vm;
