package rs.emr.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import rs.emr.domain.RecordType;
import rs.emr.repository.RecordTypeRepository;
import rs.emr.service.RecordTypeService;
import rs.emr.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link rs.emr.domain.RecordType}.
 */
@RestController
@RequestMapping("/api")
public class RecordTypeResource {

    private final Logger log = LoggerFactory.getLogger(RecordTypeResource.class);

    private static final String ENTITY_NAME = "recordType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecordTypeService recordTypeService;

    private final RecordTypeRepository recordTypeRepository;

    public RecordTypeResource(RecordTypeService recordTypeService, RecordTypeRepository recordTypeRepository) {
        this.recordTypeService = recordTypeService;
        this.recordTypeRepository = recordTypeRepository;
    }

    /**
     * {@code POST  /record-types} : Create a new recordType.
     *
     * @param recordType the recordType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recordType, or with status {@code 400 (Bad Request)} if the recordType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/record-types")
    public ResponseEntity<RecordType> createRecordType(@Valid @RequestBody RecordType recordType) throws URISyntaxException {
        log.debug("REST request to save RecordType : {}", recordType);
        if (recordType.getId() != null) {
            throw new BadRequestAlertException("A new recordType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecordType result = recordTypeService.save(recordType);
        return ResponseEntity
            .created(new URI("/api/record-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /record-types/:id} : Updates an existing recordType.
     *
     * @param id the id of the recordType to save.
     * @param recordType the recordType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recordType,
     * or with status {@code 400 (Bad Request)} if the recordType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recordType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/record-types/{id}")
    public ResponseEntity<RecordType> updateRecordType(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody RecordType recordType
    ) throws URISyntaxException {
        log.debug("REST request to update RecordType : {}, {}", id, recordType);
        if (recordType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, recordType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!recordTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RecordType result = recordTypeService.save(recordType);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, recordType.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /record-types/:id} : Partial updates given fields of an existing recordType, field will ignore if it is null
     *
     * @param id the id of the recordType to save.
     * @param recordType the recordType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recordType,
     * or with status {@code 400 (Bad Request)} if the recordType is not valid,
     * or with status {@code 404 (Not Found)} if the recordType is not found,
     * or with status {@code 500 (Internal Server Error)} if the recordType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/record-types/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RecordType> partialUpdateRecordType(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody RecordType recordType
    ) throws URISyntaxException {
        log.debug("REST request to partial update RecordType partially : {}, {}", id, recordType);
        if (recordType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, recordType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!recordTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RecordType> result = recordTypeService.partialUpdate(recordType);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, recordType.getId().toString())
        );
    }

    /**
     * {@code GET  /record-types} : get all the recordTypes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recordTypes in body.
     */
    @GetMapping("/record-types")
    public ResponseEntity<List<RecordType>> getAllRecordTypes(Pageable pageable) {
        log.debug("REST request to get a page of RecordTypes");
        Page<RecordType> page = recordTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /record-types/:id} : get the "id" recordType.
     *
     * @param id the id of the recordType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recordType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/record-types/{id}")
    public ResponseEntity<RecordType> getRecordType(@PathVariable Long id) {
        log.debug("REST request to get RecordType : {}", id);
        Optional<RecordType> recordType = recordTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recordType);
    }

    /**
     * {@code DELETE  /record-types/:id} : delete the "id" recordType.
     *
     * @param id the id of the recordType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/record-types/{id}")
    public ResponseEntity<Void> deleteRecordType(@PathVariable Long id) {
        log.debug("REST request to delete RecordType : {}", id);
        recordTypeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
