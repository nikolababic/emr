package rs.emr.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import rs.emr.domain.Consent;
import rs.emr.repository.ConsentRepository;
import rs.emr.service.ConsentService;
import rs.emr.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link rs.emr.domain.Consent}.
 */
@RestController
@RequestMapping("/api")
public class ConsentResource {

    private final Logger log = LoggerFactory.getLogger(ConsentResource.class);

    private static final String ENTITY_NAME = "consent";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ConsentService consentService;

    private final ConsentRepository consentRepository;

    public ConsentResource(ConsentService consentService, ConsentRepository consentRepository) {
        this.consentService = consentService;
        this.consentRepository = consentRepository;
    }

    /**
     * {@code POST  /consents} : Create a new consent.
     *
     * @param consent the consent to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new consent, or with status {@code 400 (Bad Request)} if the consent has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/consents")
    public ResponseEntity<Consent> createConsent(@RequestBody Consent consent) throws URISyntaxException {
        log.debug("REST request to save Consent : {}", consent);
        if (consent.getId() != null) {
            throw new BadRequestAlertException("A new consent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Consent result = consentService.save(consent);
        return ResponseEntity
            .created(new URI("/api/consents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /consents/:id} : Updates an existing consent.
     *
     * @param id the id of the consent to save.
     * @param consent the consent to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated consent,
     * or with status {@code 400 (Bad Request)} if the consent is not valid,
     * or with status {@code 500 (Internal Server Error)} if the consent couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/consents/{id}")
    public ResponseEntity<Consent> updateConsent(@PathVariable(value = "id", required = false) final Long id, @RequestBody Consent consent)
        throws URISyntaxException {
        log.debug("REST request to update Consent : {}, {}", id, consent);
        if (consent.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, consent.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!consentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Consent result = consentService.save(consent);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, consent.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /consents/:id} : Partial updates given fields of an existing consent, field will ignore if it is null
     *
     * @param id the id of the consent to save.
     * @param consent the consent to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated consent,
     * or with status {@code 400 (Bad Request)} if the consent is not valid,
     * or with status {@code 404 (Not Found)} if the consent is not found,
     * or with status {@code 500 (Internal Server Error)} if the consent couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/consents/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Consent> partialUpdateConsent(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Consent consent
    ) throws URISyntaxException {
        log.debug("REST request to partial update Consent partially : {}, {}", id, consent);
        if (consent.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, consent.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!consentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Consent> result = consentService.partialUpdate(consent);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, consent.getId().toString())
        );
    }

    /**
     * {@code GET  /consents} : get all the consents.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of consents in body.
     */
    @GetMapping("/consents")
    public ResponseEntity<List<Consent>> getAllConsents(Pageable pageable) {
        log.debug("REST request to get a page of Consents");
        Page<Consent> page = consentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/consents-doctor")
    public ResponseEntity<List<Consent>> getForDoctor() {
        log.debug("REST request to get a page of Consents");
        List<Consent> page = consentService.findForDoctor();
        return ResponseEntity.ok().body(page);
    }
    

    @GetMapping("/consents-doctor-all")
    public ResponseEntity<List<Map<String, ?>>> getForDoctorAll() {
        log.debug("REST request to get a page of Consents");
        List<Map<String, ?>> page = consentService.findAllForDoctor();
        return ResponseEntity.ok().body(page);
    }


    /**
     * {@code GET  /consents/:id} : get the "id" consent.
     *
     * @param id the id of the consent to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the consent, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/consents/{id}")
    public ResponseEntity<Consent> getConsent(@PathVariable Long id) {
        log.debug("REST request to get Consent : {}", id);
        Optional<Consent> consent = consentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(consent);
    }

    /**
     * {@code DELETE  /consents/:id} : delete the "id" consent.
     *
     * @param id the id of the consent to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/consents/{id}")
    public ResponseEntity<Void> deleteConsent(@PathVariable Long id) {
        log.debug("REST request to delete Consent : {}", id);
        consentService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
