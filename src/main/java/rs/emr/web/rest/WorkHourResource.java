package rs.emr.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import rs.emr.domain.WorkHour;
import rs.emr.repository.WorkHourRepository;
import rs.emr.service.WorkHourService;
import rs.emr.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link rs.emr.domain.WorkHour}.
 */
@RestController
@RequestMapping("/api")
public class WorkHourResource {

    private final Logger log = LoggerFactory.getLogger(WorkHourResource.class);

    private static final String ENTITY_NAME = "workHour";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WorkHourService workHourService;

    private final WorkHourRepository workHourRepository;

    public WorkHourResource(WorkHourService workHourService, WorkHourRepository workHourRepository) {
        this.workHourService = workHourService;
        this.workHourRepository = workHourRepository;
    }

    /**
     * {@code POST  /work-hours} : Create a new workHour.
     *
     * @param workHour the workHour to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new workHour, or with status {@code 400 (Bad Request)} if the workHour has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/work-hours")
    public ResponseEntity<WorkHour> createWorkHour(@Valid @RequestBody WorkHour workHour) throws URISyntaxException {
        log.debug("REST request to save WorkHour : {}", workHour);
        if (workHour.getId() != null) {
            throw new BadRequestAlertException("A new workHour cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WorkHour result = workHourService.save(workHour);
        return ResponseEntity
            .created(new URI("/api/work-hours/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /work-hours/:id} : Updates an existing workHour.
     *
     * @param id the id of the workHour to save.
     * @param workHour the workHour to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated workHour,
     * or with status {@code 400 (Bad Request)} if the workHour is not valid,
     * or with status {@code 500 (Internal Server Error)} if the workHour couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/work-hours/{id}")
    public ResponseEntity<WorkHour> updateWorkHour(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody WorkHour workHour
    ) throws URISyntaxException {
        log.debug("REST request to update WorkHour : {}, {}", id, workHour);
        if (workHour.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, workHour.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!workHourRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        WorkHour result = workHourService.save(workHour);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, workHour.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /work-hours/:id} : Partial updates given fields of an existing workHour, field will ignore if it is null
     *
     * @param id the id of the workHour to save.
     * @param workHour the workHour to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated workHour,
     * or with status {@code 400 (Bad Request)} if the workHour is not valid,
     * or with status {@code 404 (Not Found)} if the workHour is not found,
     * or with status {@code 500 (Internal Server Error)} if the workHour couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/work-hours/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<WorkHour> partialUpdateWorkHour(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody WorkHour workHour
    ) throws URISyntaxException {
        log.debug("REST request to partial update WorkHour partially : {}, {}", id, workHour);
        if (workHour.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, workHour.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!workHourRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<WorkHour> result = workHourService.partialUpdate(workHour);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, workHour.getId().toString())
        );
    }

    /**
     * {@code GET  /work-hours} : get all the workHours.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of workHours in body.
     */
    @GetMapping("/work-hours")
    public ResponseEntity<List<WorkHour>> getAllWorkHours(Pageable pageable) {
        log.debug("REST request to get a page of WorkHours");
        Page<WorkHour> page = workHourService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /work-hours/:id} : get the "id" workHour.
     *
     * @param id the id of the workHour to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the workHour, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/work-hours/{id}")
    public ResponseEntity<WorkHour> getWorkHour(@PathVariable Long id) {
        log.debug("REST request to get WorkHour : {}", id);
        Optional<WorkHour> workHour = workHourService.findOne(id);
        return ResponseUtil.wrapOrNotFound(workHour);
    }

    /**
     * {@code DELETE  /work-hours/:id} : delete the "id" workHour.
     *
     * @param id the id of the workHour to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/work-hours/{id}")
    public ResponseEntity<Void> deleteWorkHour(@PathVariable Long id) {
        log.debug("REST request to delete WorkHour : {}", id);
        workHourService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
