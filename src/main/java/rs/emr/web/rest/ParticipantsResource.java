package rs.emr.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import rs.emr.domain.Participants;
import rs.emr.repository.ParticipantsRepository;
import rs.emr.service.ParticipantsService;
import rs.emr.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link rs.emr.domain.Participants}.
 */
@RestController
@RequestMapping("/api")
public class ParticipantsResource {

    private final Logger log = LoggerFactory.getLogger(ParticipantsResource.class);

    private static final String ENTITY_NAME = "participants";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ParticipantsService participantsService;

    private final ParticipantsRepository participantsRepository;

    public ParticipantsResource(ParticipantsService participantsService, ParticipantsRepository participantsRepository) {
        this.participantsService = participantsService;
        this.participantsRepository = participantsRepository;
    }

    /**
     * {@code POST  /participants} : Create a new participants.
     *
     * @param participants the participants to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new participants, or with status {@code 400 (Bad Request)} if the participants has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/participants")
    public ResponseEntity<Participants> createParticipants(@RequestBody Participants participants) throws URISyntaxException {
        log.debug("REST request to save Participants : {}", participants);
        if (participants.getId() != null) {
            throw new BadRequestAlertException("A new participants cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Participants result = participantsService.save(participants);
        return ResponseEntity
            .created(new URI("/api/participants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /participants/:id} : Updates an existing participants.
     *
     * @param id the id of the participants to save.
     * @param participants the participants to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated participants,
     * or with status {@code 400 (Bad Request)} if the participants is not valid,
     * or with status {@code 500 (Internal Server Error)} if the participants couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/participants/{id}")
    public ResponseEntity<Participants> updateParticipants(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Participants participants
    ) throws URISyntaxException {
        log.debug("REST request to update Participants : {}, {}", id, participants);
        if (participants.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, participants.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!participantsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Participants result = participantsService.save(participants);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, participants.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /participants/:id} : Partial updates given fields of an existing participants, field will ignore if it is null
     *
     * @param id the id of the participants to save.
     * @param participants the participants to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated participants,
     * or with status {@code 400 (Bad Request)} if the participants is not valid,
     * or with status {@code 404 (Not Found)} if the participants is not found,
     * or with status {@code 500 (Internal Server Error)} if the participants couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/participants/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Participants> partialUpdateParticipants(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Participants participants
    ) throws URISyntaxException {
        log.debug("REST request to partial update Participants partially : {}, {}", id, participants);
        if (participants.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, participants.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!participantsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Participants> result = participantsService.partialUpdate(participants);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, participants.getId().toString())
        );
    }

    /**
     * {@code GET  /participants} : get all the participants.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of participants in body.
     */
    @GetMapping("/participants")
    public ResponseEntity<List<Participants>> getAllParticipants(Pageable pageable) {
        log.debug("REST request to get a page of Participants");
        Page<Participants> page = participantsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /participants/:id} : get the "id" participants.
     *
     * @param id the id of the participants to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the participants, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/participants/{id}")
    public ResponseEntity<Participants> getParticipants(@PathVariable Long id) {
        log.debug("REST request to get Participants : {}", id);
        Optional<Participants> participants = participantsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(participants);
    }

    /**
     * {@code DELETE  /participants/:id} : delete the "id" participants.
     *
     * @param id the id of the participants to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/participants/{id}")
    public ResponseEntity<Void> deleteParticipants(@PathVariable Long id) {
        log.debug("REST request to delete Participants : {}", id);
        participantsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
