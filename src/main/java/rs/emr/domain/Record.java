package rs.emr.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Record.
 */
@Entity
@Table(name = "record")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Record implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "record_date")
    private Instant recordDate;

    @Column(name = "additional_information")
    private String additionalInformation;

    @Column(name = "active")
    private Boolean active;

    @ManyToOne
    private User user;

    @ManyToOne
    private RecordType recordType;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Record id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Record name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getRecordDate() {
        return this.recordDate;
    }

    public Record recordDate(Instant recordDate) {
        this.setRecordDate(recordDate);
        return this;
    }

    public void setRecordDate(Instant recordDate) {
        this.recordDate = recordDate;
    }

    public String getAdditionalInformation() {
        return this.additionalInformation;
    }

    public Record additionalInformation(String additionalInformation) {
        this.setAdditionalInformation(additionalInformation);
        return this;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public Boolean getActive() {
        return this.active;
    }

    public Record active(Boolean active) {
        this.setActive(active);
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Record user(User user) {
        this.setUser(user);
        return this;
    }

    public RecordType getRecordType() {
        return this.recordType;
    }

    public void setRecordType(RecordType recordType) {
        this.recordType = recordType;
    }

    public Record recordType(RecordType recordType) {
        this.setRecordType(recordType);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Record)) {
            return false;
        }
        return id != null && id.equals(((Record) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Record{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", recordDate='" + getRecordDate() + "'" +
            ", additionalInformation='" + getAdditionalInformation() + "'" +
            ", active='" + getActive() + "'" +
            "}";
    }
}
