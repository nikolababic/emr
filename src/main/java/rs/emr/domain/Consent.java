package rs.emr.domain;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Consent.
 */
@Entity
@Table(name = "consent")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Consent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "consent_status")
    private Boolean consentStatus;

    @ManyToOne
    private User patient;

    @ManyToOne
    private User doctor;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Consent id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getConsentStatus() {
        return this.consentStatus;
    }

    public Consent consentStatus(Boolean consentStatus) {
        this.setConsentStatus(consentStatus);
        return this;
    }

    public void setConsentStatus(Boolean consentStatus) {
        this.consentStatus = consentStatus;
    }

    public User getPatient() {
        return this.patient;
    }

    public void setPatient(User user) {
        this.patient = user;
    }

    public Consent patient(User user) {
        this.setPatient(user);
        return this;
    }

    public User getDoctor() {
        return this.doctor;
    }

    public void setDoctor(User user) {
        this.doctor = user;
    }

    public Consent doctor(User user) {
        this.setDoctor(user);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Consent)) {
            return false;
        }
        return id != null && id.equals(((Consent) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Consent{" +
            "id=" + getId() +
            ", consentStatus='" + getConsentStatus() + "'" +
            "}";
    }
}
