package rs.emr.domain;

import java.time.Instant;

import org.springframework.web.multipart.MultipartFile;

public class FormWrapper {
	private MultipartFile file;
    private String name;
    private Instant documentDate;
    private String note;
    private String recordId;
    private String userId;
    
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Instant getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(Instant documentDate) {
		this.documentDate = documentDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
    
    
}
