package rs.emr.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A RecordType.
 */
@Entity
@Table(name = "record_type")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RecordType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(max = 63)
    @Column(name = "name", length = 63, nullable = false)
    private String name;

    @Column(name = "visible_columns")
    private String visibleColumns;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public RecordType id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public RecordType name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVisibleColumns() {
        return this.visibleColumns;
    }

    public RecordType visibleColumns(String visibleColumns) {
        this.setVisibleColumns(visibleColumns);
        return this;
    }

    public void setVisibleColumns(String visibleColumns) {
        this.visibleColumns = visibleColumns;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RecordType)) {
            return false;
        }
        return id != null && id.equals(((RecordType) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RecordType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", visibleColumns='" + getVisibleColumns() + "'" +
            "}";
    }
}
