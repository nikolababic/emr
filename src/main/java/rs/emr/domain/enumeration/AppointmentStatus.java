package rs.emr.domain.enumeration;

/**
 * The AppointmentStatus enumeration.
 */
public enum AppointmentStatus {
    APPROVED,
    REJECTED,
    PENDING,
}
