package rs.emr.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE,
    FEMALE,
}
