package rs.emr.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.emr.domain.Message;

/**
 * Spring Data SQL repository for the Message entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
    @Query("select message from Message message where message.user.login = ?#{principal.username}")
    List<Message> findByUserIsCurrentUser();
    
    @Query(value = "SELECT m.id, m.message_text as messageText, m.user_id as userId, m.message_date as messageDate, CONCAT(eu.first_name, ' ', eu.last_name) as user " + 
    		"FROM message m " + 
    		"INNER JOIN participants p ON p.conversation_id = m.conversation_id " + 
    		"INNER JOIN emr_user eu ON eu.id = m.user_id " + 
    		"WHERE m.conversation_id = :convesationId " + 
    		"GROUP BY m.id " + 
    		"ORDER BY m.message_date ASC ", nativeQuery = true)
    List<Map<String, ?>> listMessagesForConvesation(@Param("convesationId") Long convesationId);
}
