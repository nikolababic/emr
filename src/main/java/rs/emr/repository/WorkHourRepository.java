package rs.emr.repository;

import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import rs.emr.domain.WorkHour;

/**
 * Spring Data SQL repository for the WorkHour entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WorkHourRepository extends JpaRepository<WorkHour, Long> {
    @Query("select workHour from WorkHour workHour where workHour.user.login = ?#{principal.username}")
    List<WorkHour> findByUserIsCurrentUser();
}
