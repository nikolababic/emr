package rs.emr.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import rs.emr.domain.Consent;

/**
 * Spring Data SQL repository for the Consent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConsentRepository extends JpaRepository<Consent, Long> {
    @Query("select consent from Consent consent where consent.patient.login = ?#{principal.username}")
    List<Consent> findByPatientIsCurrentUser();

    @Query("select consent from Consent consent where consent.doctor.login = ?#{principal.username} and consent.consentStatus = true")
    List<Consent> findByDoctorIsCurrentUser();
    

    @Query(value = "select c.consent_status from consent c " +
    		"inner join app_user au on au.id = c.patient_id " +
    		"inner join emr_user u on c.doctor_id = u.id " + 
    		" where u.login = ?#{principal.login} and c.consent_status = true", nativeQuery = true)
    List<Map<String, ?>> findAllForDoctor();
}
