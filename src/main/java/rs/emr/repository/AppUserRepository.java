package rs.emr.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.emr.domain.AppUser;

/**
 * Spring Data SQL repository for the AppUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long>, JpaSpecificationExecutor<AppUser> {
	
	@Query(value = "SELECT au.specialization FROM AppUser au WHERE au.specialization IS NOT NULL AND au.specialization <> '' GROUP BY au.specialization")
	List<String> getSpecializations();
	
	@Query(value = "SELECT " + 
			"	au.id, " + 
			"	au.gender, " + 
			"	au.phone_number, " + 
			"	au.specialization, " + 
			"	eu.first_name, " + 
			"	eu.last_name, " + 
			"	eu.image_url, " + 
			"	i.name, " + 
			"	a.latitude, " + 
			"	a.longitude, " + 
			"	a.street, " + 
			"	a.city " + 
			"FROM " + 
			"	app_user au " + 
			"LEFT JOIN emr_user eu ON " + 
			"	eu.id = au.id " + 
			"LEFT JOIN emr_user_authority eua ON " + 
			"	eua.user_id = eu.id " + 
			"INNER JOIN institution i ON " + 
			"	au.institution_id = i.id " + 
			"INNER JOIN address a ON " + 
			"	a.id = i.address_id " + 
			"WHERE " + 
			"	eua.authority_name = 'ROLE_DOCTOR' " + 
			"", nativeQuery = true)
	List<Map<String, ?>> searchDoctors();
	
	@Modifying
	@Query(value = "UPDATE app_user SET money = money - :money WHERE id = :id", nativeQuery = true)
	void updateMoney(@Param("id") Long id, @Param("money") Double money);
	
}
