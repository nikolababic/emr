package rs.emr.repository;

import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import rs.emr.domain.Record;

/**
 * Spring Data SQL repository for the Record entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecordRepository extends JpaRepository<Record, Long>, JpaSpecificationExecutor<Record> {
    @Query("select record from Record record where record.user.login = ?#{principal.username}")
    List<Record> findByUserIsCurrentUser();
}
