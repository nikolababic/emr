package rs.emr.repository;

import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import rs.emr.domain.Participants;

/**
 * Spring Data SQL repository for the Participants entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParticipantsRepository extends JpaRepository<Participants, Long> {
    @Query("select participants from Participants participants where participants.user.login = ?#{principal.username}")
    List<Participants> findByUserIsCurrentUser();
}
