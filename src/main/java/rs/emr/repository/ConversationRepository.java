package rs.emr.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.emr.domain.Conversation;

/**
 * Spring Data SQL repository for the Conversation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConversationRepository extends JpaRepository<Conversation, Long> {
	@Query(value = "SELECT  c.id as id, c.name, p.seen, CONCAT(eu.first_name, ' ', eu.last_name) as user_name FROM conversation c "
				+ "INNER JOIN participants p ON c.id = p.conversation_id "
				+ "INNER JOIN emr_user eu ON eu.id = p.user_id "
				+ "WHERE p.user_id = :userId"
				+ " ORDER BY p.seen, c.id DESC",
			nativeQuery = true)
	List<Map<String,?>> getMyConversations(@Param("userId") Long userId);
}
