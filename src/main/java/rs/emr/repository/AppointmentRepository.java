package rs.emr.repository;

import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import rs.emr.domain.Appointment;

/**
 * Spring Data SQL repository for the Appointment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
    @Query("select appointment from Appointment appointment where appointment.patient.login = ?#{principal.username}")
    List<Appointment> findByPatientIsCurrentUser();

    @Query("select appointment from Appointment appointment where appointment.doctor.login = ?#{principal.username}")
    List<Appointment> findByDoctorIsCurrentUser();
}
