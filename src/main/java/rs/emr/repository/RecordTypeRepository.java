package rs.emr.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import rs.emr.domain.RecordType;

/**
 * Spring Data SQL repository for the RecordType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecordTypeRepository extends JpaRepository<RecordType, Long> {}
