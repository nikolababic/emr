package rs.emr.repository;

import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import rs.emr.domain.Transaction;

/**
 * Spring Data SQL repository for the Transaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>, JpaSpecificationExecutor<Transaction> {
    @Query("select transaction from Transaction transaction where transaction.patient.login = ?#{principal.username}")
    List<Transaction> findByPatientIsCurrentUser();

    @Query("select transaction from Transaction transaction where transaction.administrator.login = ?#{principal.username}")
    List<Transaction> findByAdministratorIsCurrentUser();
}
