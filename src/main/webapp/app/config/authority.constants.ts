export enum Authority {
  ADMIN = 'ROLE_ADMIN',
  DOCTOR = 'ROLE_DOCTOR',
  PATIENT = 'ROLE_PATIENT',
}
