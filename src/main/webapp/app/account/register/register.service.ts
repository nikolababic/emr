import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { Registration } from './register.model';
import { DATE_FORMAT } from 'app/config/input.constants';

@Injectable({ providedIn: 'root' })
export class RegisterService {
  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  save(registration: Registration): Observable<{}> {
    const copy = this.convertDateFromClient(registration);
    return this.http.post(this.applicationConfigService.getEndpointFor('api/register'), copy);
  }

  protected convertDateFromClient(registration: Registration): Registration {
    return Object.assign({}, registration, {
      dateOfBirth: registration.dateOfBirth?.isValid() ? registration.dateOfBirth.format(DATE_FORMAT) : undefined,
    });
  }
}
