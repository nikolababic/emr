import * as dayjs from 'dayjs';

export class Registration {
  constructor(
    public login: string,
    public firstName: string,
    public lastName: string,
    public email: string,
    public password: string,
    public langKey: string,
    public gender?: string,
    public dateOfBirth?: dayjs.Dayjs,
    public phoneNumber?: string
  ) {}
}
