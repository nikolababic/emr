import * as dayjs from 'dayjs';
import { IUser } from 'app/entities/user/user.model';
import { IRecord } from 'app/entities/record/record.model';

export interface IDocument {
  id?: number;
  name?: string | null;
  file?: string;
  documentDate?: dayjs.Dayjs | null;
  note?: string | null;
  user?: IUser | null;
  record?: IRecord | null;
}

export class Document implements IDocument {
  constructor(
    public id?: number,
    public name?: string | null,
    public file?: string,
    public documentDate?: dayjs.Dayjs | null,
    public note?: string | null,
    public user?: IUser | null,
    public record?: IRecord | null
  ) {}
}

export function getDocumentIdentifier(document: IDocument): number | undefined {
  return document.id;
}
