import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IDocument, Document } from '../document.model';
import { DocumentService } from '../service/document.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { IRecord } from 'app/entities/record/record.model';
import { RecordService } from 'app/entities/record/service/record.service';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'emr-document-update',
  templateUrl: './document-update.component.html',
})
export class DocumentUpdateComponent implements OnInit {
  isSaving = false;
  userId?: string;

  recordsSharedCollection: IRecord[] = [];
  filesNew: string[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.maxLength(255)]],
    file: [null, [Validators.required]],
    documentDate: [],
    note: [],
    user: [],
    record: [],
  });

  constructor(
    protected documentService: DocumentService,
    protected userService: UserService,
    private accountService: AccountService,
    protected recordService: RecordService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ document }) => {
      if (document.id === undefined) {
        const today = dayjs().startOf('day');
        document.documentDate = today;
      }

      this.userId = this.activatedRoute.snapshot.paramMap.get('user') ?? '';
      if (this.userId === 'me') {
        this.accountService.getAuthenticationState().subscribe(account => (this.userId = String(account?.id)));
      }

      this.updateForm(document);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const document = this.createFromForm();



    if (document.id !== undefined) {
      this.subscribeToSaveResponse(this.documentService.update(document));
    } else {
      this.subscribeToSaveResponse(this.documentService.create(document, this.filesNew));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  trackRecordById(index: number, item: IRecord): number {
    return item.id!;
  }

  onFileChange(event: any): void {
    if (event?.target?.files && event?.target?.files?.length) {
      const file = event.target.files;
      this.filesNew = file;
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDocument>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(document: IDocument): void {
    this.editForm.patchValue({
      id: document.id,
      name: document.name,
      file: document.file,
      documentDate: document.documentDate ? document.documentDate.format(DATE_TIME_FORMAT) : null,
      note: document.note,
      user: document.user,
      record: document.record,
    });

    this.recordsSharedCollection = this.recordService.addRecordToCollectionIfMissing(this.recordsSharedCollection, document.record);
  }

  protected loadRelationshipsOptions(): void {
    this.recordService
      .query({'userId.equals': this.userId})
      .pipe(map((res: HttpResponse<IRecord[]>) => res.body ?? []))
      .pipe(map((records: IRecord[]) => this.recordService.addRecordToCollectionIfMissing(records, this.editForm.get('record')!.value)))
      .subscribe((records: IRecord[]) => (this.recordsSharedCollection = records));
  }

  protected createFromForm(): IDocument {
    return {
      ...new Document(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      file: this.editForm.get(['file'])!.value,
      documentDate: this.editForm.get(['documentDate'])!.value
        ? dayjs(this.editForm.get(['documentDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      note: this.editForm.get(['note'])!.value,
      user: { id: Number(this.userId) },
      record: this.editForm.get(['record'])!.value,
    };
  }
}
