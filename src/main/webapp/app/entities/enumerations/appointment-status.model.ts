export enum AppointmentStatus {
  APPROVED = 'APPROVED',

  REJECTED = 'REJECTED',

  PENDING = 'PENDING',
}
