import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IAppUser, AppUser } from '../app-user.model';
import { AppUserService } from '../service/app-user.service';
import { IUser, User } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { IInstitution } from 'app/entities/institution/institution.model';
import { InstitutionService } from 'app/entities/institution/service/institution.service';
import { LANGUAGES } from 'app/config/language.constants';

@Component({
  selector: 'emr-app-user-update',
  templateUrl: './app-user-update.component.html',
})
export class AppUserUpdateComponent implements OnInit {
  isSaving = false;
  languages = LANGUAGES;
  authorities: string[] = [];
  username = '';
  user!: User;
  institutionsSharedCollection: IInstitution[] = [];
  specializations: string[] = [];

  editForm = this.fb.group({
    id: [],
    phoneNumber: [null, [Validators.maxLength(20)]],
    dateOfBirth: [],
    gender: [],
    title: [null, [Validators.maxLength(63)]],
    specialization: [null, [Validators.maxLength(255)]],
    money: [],
    notes: [],
    user: [],
    institution: [],
  });

  editAdminForm = this.fb.group({
    id: [],
    login: [
      '',
      [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50),
        Validators.pattern('^[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$|^[_.@A-Za-z0-9-]+$'),
      ],
    ],
    firstName: ['', [Validators.maxLength(50)]],
    lastName: ['', [Validators.maxLength(50)]],
    email: ['', [Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    activated: [],
    langKey: [],
    authorities: [],
  })
  constructor(
    protected appUserService: AppUserService,
    protected userService: UserService,
    protected institutionService: InstitutionService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ appUser }) => {
      this.updateForm(appUser);
      if (appUser.id) {
        this.username = appUser.user?.login;
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const appUser = this.createFromForm();
    if (appUser.id !== undefined) {
      this.subscribeToSaveResponse(this.appUserService.update(appUser));
    } else {
      this.subscribeToSaveResponse(this.appUserService.create(appUser));
    }
  }

  saveAdmin(): void {
    this.isSaving = true;
    this.createAdminUser(this.user);
    if (this.user.id !== undefined) {
      this.appUserService.updateUser(this.user).subscribe(
        () => this.onSaveAdminSuccess(),
        () => this.onSaveError()
      );
    } else {
      this.appUserService.updateUser(this.user).subscribe(
        () => this.onSaveAdminSuccess(),
        () => this.onSaveError()
      );
    }

  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  trackInstitutionById(index: number, item: IInstitution): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAppUser>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveAdminSuccess(): void {
    this.save();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(appUser: IAppUser): void {
    this.editForm.patchValue({
      id: appUser.id,
      phoneNumber: appUser.phoneNumber,
      dateOfBirth: appUser.dateOfBirth,
      gender: appUser.gender,
      title: appUser.title,
      specialization: appUser.specialization,
      money: appUser.money,
      notes: appUser.notes,
      user: appUser.user,
      institution: appUser.institution,
    });

    this.institutionsSharedCollection = this.institutionService.addInstitutionToCollectionIfMissing(
      this.institutionsSharedCollection,
      appUser.institution
    );

  }

  protected updateAdminForm(user: IUser): void {
    this.user = user;
    this.editAdminForm.patchValue({
      id: user.id,
      login: user.login,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      activated: user.activated,
      langKey: user.langKey,
      authorities: user.authorities,
    });
  }

  protected loadRelationshipsOptions(): void {
    this.appUserService.getSpecializations().subscribe(data => {
      this.specializations = data;
    });

    this.institutionService
      .query()
      .pipe(map((res: HttpResponse<IInstitution[]>) => res.body ?? []))
      .pipe(
        map((institutions: IInstitution[]) =>
          this.institutionService.addInstitutionToCollectionIfMissing(institutions, this.editForm.get('institution')!.value)
        )
      )
      .subscribe((institutions: IInstitution[]) => (this.institutionsSharedCollection = institutions));
      
      this.appUserService.authorities().subscribe(authorities => (this.authorities = authorities));
    
      if (this.username.length) {
        this.appUserService.findUser(this.username).subscribe(
          (user: IUser) => this.updateAdminForm(user)
        );
      }


    }

  protected createFromForm(): IAppUser {
    return {
      ...new AppUser(),
      id: this.editForm.get(['id'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      dateOfBirth: this.editForm.get(['dateOfBirth'])!.value,
      gender: this.editForm.get(['gender'])!.value,
      title: this.editForm.get(['title'])!.value,
      specialization: this.editForm.get(['specialization'])!.value,
      money: this.editForm.get(['money'])!.value,
      notes: this.editForm.get(['notes'])!.value,
      user: this.editForm.get(['user'])!.value,
      institution: this.editForm.get(['institution'])!.value,
    };
  }

  protected createAdminUser(user: User): void {
    user.login = this.editAdminForm.get(['login'])!.value;
    user.firstName = this.editAdminForm.get(['firstName'])!.value;
    user.lastName = this.editAdminForm.get(['lastName'])!.value;
    user.email = this.editAdminForm.get(['email'])!.value;
    user.activated = this.editAdminForm.get(['activated'])!.value;
    if (typeof(this.editAdminForm.get(['authorities'])!.value) === 'object') {
      user.authorities = this.editAdminForm.get(['authorities'])!.value;
    } else {
      user.authorities = [this.editAdminForm.get(['authorities'])!.value];
    }
  }
}
