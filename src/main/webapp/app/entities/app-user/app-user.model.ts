import * as dayjs from 'dayjs';
import { IUser } from 'app/entities/user/user.model';
import { IInstitution } from 'app/entities/institution/institution.model';
import { Gender } from 'app/entities/enumerations/gender.model';

export interface IAppUser {
  id?: number;
  phoneNumber?: string | null;
  dateOfBirth?: dayjs.Dayjs | null;
  gender?: Gender | null;
  title?: string | null;
  specialization?: string | null;
  money?: number | null;
  notes?: string | null;
  user?: IUser | null;
  institution?: IInstitution | null;
}

export class AppUser implements IAppUser {
  constructor(
    public id?: number,
    public phoneNumber?: string | null,
    public dateOfBirth?: dayjs.Dayjs | null,
    public gender?: Gender | null,
    public title?: string | null,
    public specialization?: string | null,
    public money?: number | null,
    public notes?: string | null,
    public user?: IUser | null,
    public institution?: IInstitution | null
  ) {}
}

export function getAppUserIdentifier(appUser: IAppUser): number | undefined {
  return appUser.id;
}
