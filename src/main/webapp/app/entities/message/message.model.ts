import * as dayjs from 'dayjs';
import { IUser } from 'app/entities/user/user.model';
import { IConversation } from 'app/entities/conversation/conversation.model';

export interface IMessage {
  id?: number;
  messageText?: string | null;
  messageDate?: dayjs.Dayjs | null;
  user?: IUser | null;
  conversation?: IConversation | null;
}

export class Message implements IMessage {
  constructor(
    public id?: number,
    public messageText?: string | null,
    public messageDate?: dayjs.Dayjs | null,
    public user?: IUser | null,
    public conversation?: IConversation | null
  ) {}
}

export function getMessageIdentifier(message: IMessage): number | undefined {
  return message.id;
}
