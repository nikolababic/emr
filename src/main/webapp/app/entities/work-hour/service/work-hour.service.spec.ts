import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IWorkHour, WorkHour } from '../work-hour.model';

import { WorkHourService } from './work-hour.service';

describe('Service Tests', () => {
  describe('WorkHour Service', () => {
    let service: WorkHourService;
    let httpMock: HttpTestingController;
    let elemDefault: IWorkHour;
    let expectedResult: IWorkHour | IWorkHour[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(WorkHourService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        timeFrom: 'AAAAAAA',
        timeTo: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a WorkHour', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new WorkHour()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a WorkHour', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            timeFrom: 'BBBBBB',
            timeTo: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a WorkHour', () => {
        const patchObject = Object.assign(
          {
            timeTo: 'BBBBBB',
          },
          new WorkHour()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of WorkHour', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            timeFrom: 'BBBBBB',
            timeTo: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a WorkHour', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addWorkHourToCollectionIfMissing', () => {
        it('should add a WorkHour to an empty array', () => {
          const workHour: IWorkHour = { id: 123 };
          expectedResult = service.addWorkHourToCollectionIfMissing([], workHour);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(workHour);
        });

        it('should not add a WorkHour to an array that contains it', () => {
          const workHour: IWorkHour = { id: 123 };
          const workHourCollection: IWorkHour[] = [
            {
              ...workHour,
            },
            { id: 456 },
          ];
          expectedResult = service.addWorkHourToCollectionIfMissing(workHourCollection, workHour);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a WorkHour to an array that doesn't contain it", () => {
          const workHour: IWorkHour = { id: 123 };
          const workHourCollection: IWorkHour[] = [{ id: 456 }];
          expectedResult = service.addWorkHourToCollectionIfMissing(workHourCollection, workHour);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(workHour);
        });

        it('should add only unique WorkHour to an array', () => {
          const workHourArray: IWorkHour[] = [{ id: 123 }, { id: 456 }, { id: 71428 }];
          const workHourCollection: IWorkHour[] = [{ id: 123 }];
          expectedResult = service.addWorkHourToCollectionIfMissing(workHourCollection, ...workHourArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const workHour: IWorkHour = { id: 123 };
          const workHour2: IWorkHour = { id: 456 };
          expectedResult = service.addWorkHourToCollectionIfMissing([], workHour, workHour2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(workHour);
          expect(expectedResult).toContain(workHour2);
        });

        it('should accept null and undefined values', () => {
          const workHour: IWorkHour = { id: 123 };
          expectedResult = service.addWorkHourToCollectionIfMissing([], null, workHour, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(workHour);
        });

        it('should return initial array if no WorkHour is added', () => {
          const workHourCollection: IWorkHour[] = [{ id: 123 }];
          expectedResult = service.addWorkHourToCollectionIfMissing(workHourCollection, undefined, null);
          expect(expectedResult).toEqual(workHourCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
