import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IWorkHour, getWorkHourIdentifier } from '../work-hour.model';

export type EntityResponseType = HttpResponse<IWorkHour>;
export type EntityArrayResponseType = HttpResponse<IWorkHour[]>;

@Injectable({ providedIn: 'root' })
export class WorkHourService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/work-hours');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(workHour: IWorkHour): Observable<EntityResponseType> {
    return this.http.post<IWorkHour>(this.resourceUrl, workHour, { observe: 'response' });
  }

  update(workHour: IWorkHour): Observable<EntityResponseType> {
    return this.http.put<IWorkHour>(`${this.resourceUrl}/${getWorkHourIdentifier(workHour) as number}`, workHour, { observe: 'response' });
  }

  partialUpdate(workHour: IWorkHour): Observable<EntityResponseType> {
    return this.http.patch<IWorkHour>(`${this.resourceUrl}/${getWorkHourIdentifier(workHour) as number}`, workHour, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IWorkHour>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWorkHour[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addWorkHourToCollectionIfMissing(workHourCollection: IWorkHour[], ...workHoursToCheck: (IWorkHour | null | undefined)[]): IWorkHour[] {
    const workHours: IWorkHour[] = workHoursToCheck.filter(isPresent);
    if (workHours.length > 0) {
      const workHourCollectionIdentifiers = workHourCollection.map(workHourItem => getWorkHourIdentifier(workHourItem)!);
      const workHoursToAdd = workHours.filter(workHourItem => {
        const workHourIdentifier = getWorkHourIdentifier(workHourItem);
        if (workHourIdentifier == null || workHourCollectionIdentifiers.includes(workHourIdentifier)) {
          return false;
        }
        workHourCollectionIdentifiers.push(workHourIdentifier);
        return true;
      });
      return [...workHoursToAdd, ...workHourCollection];
    }
    return workHourCollection;
  }
}
