jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { WorkHourService } from '../service/work-hour.service';
import { IWorkHour, WorkHour } from '../work-hour.model';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

import { WorkHourUpdateComponent } from './work-hour-update.component';

describe('Component Tests', () => {
  describe('WorkHour Management Update Component', () => {
    let comp: WorkHourUpdateComponent;
    let fixture: ComponentFixture<WorkHourUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let workHourService: WorkHourService;
    let userService: UserService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [WorkHourUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(WorkHourUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WorkHourUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      workHourService = TestBed.inject(WorkHourService);
      userService = TestBed.inject(UserService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call User query and add missing value', () => {
        const workHour: IWorkHour = { id: 456 };
        const user: IUser = { id: 53938 };
        workHour.user = user;

        const userCollection: IUser[] = [{ id: 43815 }];
        jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
        const additionalUsers = [user];
        const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
        jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ workHour });
        comp.ngOnInit();

        expect(userService.query).toHaveBeenCalled();
        expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(userCollection, ...additionalUsers);
        expect(comp.usersSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const workHour: IWorkHour = { id: 456 };
        const user: IUser = { id: 10842 };
        workHour.user = user;

        activatedRoute.data = of({ workHour });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(workHour));
        expect(comp.usersSharedCollection).toContain(user);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<WorkHour>>();
        const workHour = { id: 123 };
        jest.spyOn(workHourService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ workHour });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: workHour }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(workHourService.update).toHaveBeenCalledWith(workHour);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<WorkHour>>();
        const workHour = new WorkHour();
        jest.spyOn(workHourService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ workHour });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: workHour }));
        saveSubject.complete();

        // THEN
        expect(workHourService.create).toHaveBeenCalledWith(workHour);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<WorkHour>>();
        const workHour = { id: 123 };
        jest.spyOn(workHourService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ workHour });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(workHourService.update).toHaveBeenCalledWith(workHour);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackUserById', () => {
        it('Should return tracked User primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackUserById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
