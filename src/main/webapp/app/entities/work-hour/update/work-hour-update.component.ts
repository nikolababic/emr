import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IWorkHour, WorkHour } from '../work-hour.model';
import { WorkHourService } from '../service/work-hour.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

@Component({
  selector: 'emr-work-hour-update',
  templateUrl: './work-hour-update.component.html',
})
export class WorkHourUpdateComponent implements OnInit {
  isSaving = false;

  usersSharedCollection: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    timeFrom: [null, [Validators.pattern('^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')]],
    timeTo: [null, [Validators.pattern('^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')]],
    user: [],
  });

  constructor(
    protected workHourService: WorkHourService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ workHour }) => {
      this.updateForm(workHour);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const workHour = this.createFromForm();
    if (workHour.id !== undefined) {
      this.subscribeToSaveResponse(this.workHourService.update(workHour));
    } else {
      this.subscribeToSaveResponse(this.workHourService.create(workHour));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWorkHour>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(workHour: IWorkHour): void {
    this.editForm.patchValue({
      id: workHour.id,
      timeFrom: workHour.timeFrom,
      timeTo: workHour.timeTo,
      user: workHour.user,
    });

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(this.usersSharedCollection, workHour.user);
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, this.editForm.get('user')!.value)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));
  }

  protected createFromForm(): IWorkHour {
    return {
      ...new WorkHour(),
      id: this.editForm.get(['id'])!.value,
      timeFrom: this.editForm.get(['timeFrom'])!.value,
      timeTo: this.editForm.get(['timeTo'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }
}
