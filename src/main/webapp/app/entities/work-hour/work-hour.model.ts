import { IUser } from 'app/entities/user/user.model';

export interface IWorkHour {
  id?: number;
  timeFrom?: string | null;
  timeTo?: string | null;
  user?: IUser | null;
}

export class WorkHour implements IWorkHour {
  constructor(public id?: number, public timeFrom?: string | null, public timeTo?: string | null, public user?: IUser | null) {}
}

export function getWorkHourIdentifier(workHour: IWorkHour): number | undefined {
  return workHour.id;
}
