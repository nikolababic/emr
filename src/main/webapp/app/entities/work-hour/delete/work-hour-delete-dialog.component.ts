import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IWorkHour } from '../work-hour.model';
import { WorkHourService } from '../service/work-hour.service';

@Component({
  templateUrl: './work-hour-delete-dialog.component.html',
})
export class WorkHourDeleteDialogComponent {
  workHour?: IWorkHour;

  constructor(protected workHourService: WorkHourService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.workHourService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
