import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { WorkHourComponent } from './list/work-hour.component';
import { WorkHourDetailComponent } from './detail/work-hour-detail.component';
import { WorkHourUpdateComponent } from './update/work-hour-update.component';
import { WorkHourDeleteDialogComponent } from './delete/work-hour-delete-dialog.component';
import { WorkHourRoutingModule } from './route/work-hour-routing.module';

@NgModule({
  imports: [SharedModule, WorkHourRoutingModule],
  declarations: [WorkHourComponent, WorkHourDetailComponent, WorkHourUpdateComponent, WorkHourDeleteDialogComponent],
  entryComponents: [WorkHourDeleteDialogComponent],
})
export class WorkHourModule {}
