jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IWorkHour, WorkHour } from '../work-hour.model';
import { WorkHourService } from '../service/work-hour.service';

import { WorkHourRoutingResolveService } from './work-hour-routing-resolve.service';

describe('Service Tests', () => {
  describe('WorkHour routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: WorkHourRoutingResolveService;
    let service: WorkHourService;
    let resultWorkHour: IWorkHour | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(WorkHourRoutingResolveService);
      service = TestBed.inject(WorkHourService);
      resultWorkHour = undefined;
    });

    describe('resolve', () => {
      it('should return IWorkHour returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultWorkHour = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultWorkHour).toEqual({ id: 123 });
      });

      it('should return new IWorkHour if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultWorkHour = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultWorkHour).toEqual(new WorkHour());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as WorkHour })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultWorkHour = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultWorkHour).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
