import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { WorkHourComponent } from '../list/work-hour.component';
import { WorkHourDetailComponent } from '../detail/work-hour-detail.component';
import { WorkHourUpdateComponent } from '../update/work-hour-update.component';
import { WorkHourRoutingResolveService } from './work-hour-routing-resolve.service';

const workHourRoute: Routes = [
  {
    path: '',
    component: WorkHourComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: WorkHourDetailComponent,
    resolve: {
      workHour: WorkHourRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: WorkHourUpdateComponent,
    resolve: {
      workHour: WorkHourRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: WorkHourUpdateComponent,
    resolve: {
      workHour: WorkHourRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(workHourRoute)],
  exports: [RouterModule],
})
export class WorkHourRoutingModule {}
