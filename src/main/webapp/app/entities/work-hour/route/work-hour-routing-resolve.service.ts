import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IWorkHour, WorkHour } from '../work-hour.model';
import { WorkHourService } from '../service/work-hour.service';

@Injectable({ providedIn: 'root' })
export class WorkHourRoutingResolveService implements Resolve<IWorkHour> {
  constructor(protected service: WorkHourService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWorkHour> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((workHour: HttpResponse<WorkHour>) => {
          if (workHour.body) {
            return of(workHour.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new WorkHour());
  }
}
