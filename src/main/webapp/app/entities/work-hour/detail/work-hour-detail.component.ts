import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWorkHour } from '../work-hour.model';

@Component({
  selector: 'emr-work-hour-detail',
  templateUrl: './work-hour-detail.component.html',
})
export class WorkHourDetailComponent implements OnInit {
  workHour: IWorkHour | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ workHour }) => {
      this.workHour = workHour;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
