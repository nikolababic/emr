import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WorkHourDetailComponent } from './work-hour-detail.component';

describe('Component Tests', () => {
  describe('WorkHour Management Detail Component', () => {
    let comp: WorkHourDetailComponent;
    let fixture: ComponentFixture<WorkHourDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [WorkHourDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ workHour: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(WorkHourDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(WorkHourDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load workHour on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.workHour).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
