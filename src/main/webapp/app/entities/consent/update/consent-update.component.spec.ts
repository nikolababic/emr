jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ConsentService } from '../service/consent.service';
import { IConsent, Consent } from '../consent.model';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

import { ConsentUpdateComponent } from './consent-update.component';

describe('Component Tests', () => {
  describe('Consent Management Update Component', () => {
    let comp: ConsentUpdateComponent;
    let fixture: ComponentFixture<ConsentUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let consentService: ConsentService;
    let userService: UserService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ConsentUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ConsentUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ConsentUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      consentService = TestBed.inject(ConsentService);
      userService = TestBed.inject(UserService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call User query and add missing value', () => {
        const consent: IConsent = { id: 456 };
        const patient: IUser = { id: 47362 };
        consent.patient = patient;
        const doctor: IUser = { id: 52063 };
        consent.doctor = doctor;

        const userCollection: IUser[] = [{ id: 56155 }];
        jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
        const additionalUsers = [patient, doctor];
        const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
        jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ consent });
        comp.ngOnInit();

        expect(userService.query).toHaveBeenCalled();
        expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(userCollection, ...additionalUsers);
        expect(comp.usersSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const consent: IConsent = { id: 456 };
        const patient: IUser = { id: 77064 };
        consent.patient = patient;
        const doctor: IUser = { id: 40145 };
        consent.doctor = doctor;

        activatedRoute.data = of({ consent });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(consent));
        expect(comp.usersSharedCollection).toContain(patient);
        expect(comp.usersSharedCollection).toContain(doctor);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Consent>>();
        const consent = { id: 123 };
        jest.spyOn(consentService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ consent });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: consent }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(consentService.update).toHaveBeenCalledWith(consent);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Consent>>();
        const consent = new Consent();
        jest.spyOn(consentService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ consent });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: consent }));
        saveSubject.complete();

        // THEN
        expect(consentService.create).toHaveBeenCalledWith(consent);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Consent>>();
        const consent = { id: 123 };
        jest.spyOn(consentService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ consent });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(consentService.update).toHaveBeenCalledWith(consent);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackUserById', () => {
        it('Should return tracked User primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackUserById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
