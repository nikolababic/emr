import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IConsent } from '../consent.model';
import { ConsentService } from '../service/consent.service';

@Component({
  templateUrl: './consent-delete-dialog.component.html',
})
export class ConsentDeleteDialogComponent {
  consent?: IConsent;

  constructor(protected consentService: ConsentService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.consentService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
