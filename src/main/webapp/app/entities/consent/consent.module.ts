import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ConsentComponent } from './list/consent.component';
import { ConsentDetailComponent } from './detail/consent-detail.component';
import { ConsentUpdateComponent } from './update/consent-update.component';
import { ConsentDeleteDialogComponent } from './delete/consent-delete-dialog.component';
import { ConsentRoutingModule } from './route/consent-routing.module';

@NgModule({
  imports: [SharedModule, ConsentRoutingModule],
  declarations: [ConsentComponent, ConsentDetailComponent, ConsentUpdateComponent, ConsentDeleteDialogComponent],
  entryComponents: [ConsentDeleteDialogComponent],
})
export class ConsentModule {}
