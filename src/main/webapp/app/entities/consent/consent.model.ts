import { IUser } from 'app/entities/user/user.model';

export interface IConsent {
  id?: number;
  consentStatus?: boolean | null;
  patient?: IUser | null;
  doctor?: IUser | null;
}

export class Consent implements IConsent {
  constructor(public id?: number, public consentStatus?: boolean | null, public patient?: IUser | null, public doctor?: IUser | null) {
    this.consentStatus = this.consentStatus ?? false;
  }
}

export function getConsentIdentifier(consent: IConsent): number | undefined {
  return consent.id;
}
