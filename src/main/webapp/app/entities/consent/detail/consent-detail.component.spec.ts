import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ConsentDetailComponent } from './consent-detail.component';

describe('Component Tests', () => {
  describe('Consent Management Detail Component', () => {
    let comp: ConsentDetailComponent;
    let fixture: ComponentFixture<ConsentDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ConsentDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ consent: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(ConsentDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ConsentDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load consent on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.consent).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
