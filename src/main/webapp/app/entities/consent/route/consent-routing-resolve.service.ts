import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IConsent, Consent } from '../consent.model';
import { ConsentService } from '../service/consent.service';

@Injectable({ providedIn: 'root' })
export class ConsentRoutingResolveService implements Resolve<IConsent> {
  constructor(protected service: ConsentService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IConsent> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((consent: HttpResponse<Consent>) => {
          if (consent.body) {
            return of(consent.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Consent());
  }
}
