import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ConsentComponent } from '../list/consent.component';
import { ConsentDetailComponent } from '../detail/consent-detail.component';
import { ConsentUpdateComponent } from '../update/consent-update.component';
import { ConsentRoutingResolveService } from './consent-routing-resolve.service';

const consentRoute: Routes = [
  {
    path: '',
    component: ConsentComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ConsentDetailComponent,
    resolve: {
      consent: ConsentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ConsentUpdateComponent,
    resolve: {
      consent: ConsentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ConsentUpdateComponent,
    resolve: {
      consent: ConsentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(consentRoute)],
  exports: [RouterModule],
})
export class ConsentRoutingModule {}
