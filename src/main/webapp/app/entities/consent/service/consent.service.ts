import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IConsent, getConsentIdentifier } from '../consent.model';

export type EntityResponseType = HttpResponse<IConsent>;
export type EntityArrayResponseType = HttpResponse<IConsent[]>;

@Injectable({ providedIn: 'root' })
export class ConsentService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/consents');
  protected consentUrl = this.applicationConfigService.getEndpointFor('api/consents-doctor');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(consent: IConsent): Observable<EntityResponseType> {
    return this.http.post<IConsent>(this.resourceUrl, consent, { observe: 'response' });
  }

  update(consent: IConsent): Observable<EntityResponseType> {
    return this.http.put<IConsent>(`${this.resourceUrl}/${getConsentIdentifier(consent) as number}`, consent, { observe: 'response' });
  }

  partialUpdate(consent: IConsent): Observable<EntityResponseType> {
    return this.http.patch<IConsent>(`${this.resourceUrl}/${getConsentIdentifier(consent) as number}`, consent, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IConsent>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IConsent[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addConsentToCollectionIfMissing(consentCollection: IConsent[], ...consentsToCheck: (IConsent | null | undefined)[]): IConsent[] {
    const consents: IConsent[] = consentsToCheck.filter(isPresent);
    if (consents.length > 0) {
      const consentCollectionIdentifiers = consentCollection.map(consentItem => getConsentIdentifier(consentItem)!);
      const consentsToAdd = consents.filter(consentItem => {
        const consentIdentifier = getConsentIdentifier(consentItem);
        if (consentIdentifier == null || consentCollectionIdentifiers.includes(consentIdentifier)) {
          return false;
        }
        consentCollectionIdentifiers.push(consentIdentifier);
        return true;
      });
      return [...consentsToAdd, ...consentCollection];
    }
    return consentCollection;
  }

  consent(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IConsent[]>(this.consentUrl, { params: options, observe: 'response' });
  }
}
