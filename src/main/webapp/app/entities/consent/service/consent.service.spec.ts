import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IConsent, Consent } from '../consent.model';

import { ConsentService } from './consent.service';

describe('Service Tests', () => {
  describe('Consent Service', () => {
    let service: ConsentService;
    let httpMock: HttpTestingController;
    let elemDefault: IConsent;
    let expectedResult: IConsent | IConsent[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ConsentService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        consentStatus: false,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Consent', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Consent()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Consent', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            consentStatus: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Consent', () => {
        const patchObject = Object.assign({}, new Consent());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Consent', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            consentStatus: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Consent', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addConsentToCollectionIfMissing', () => {
        it('should add a Consent to an empty array', () => {
          const consent: IConsent = { id: 123 };
          expectedResult = service.addConsentToCollectionIfMissing([], consent);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(consent);
        });

        it('should not add a Consent to an array that contains it', () => {
          const consent: IConsent = { id: 123 };
          const consentCollection: IConsent[] = [
            {
              ...consent,
            },
            { id: 456 },
          ];
          expectedResult = service.addConsentToCollectionIfMissing(consentCollection, consent);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Consent to an array that doesn't contain it", () => {
          const consent: IConsent = { id: 123 };
          const consentCollection: IConsent[] = [{ id: 456 }];
          expectedResult = service.addConsentToCollectionIfMissing(consentCollection, consent);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(consent);
        });

        it('should add only unique Consent to an array', () => {
          const consentArray: IConsent[] = [{ id: 123 }, { id: 456 }, { id: 80574 }];
          const consentCollection: IConsent[] = [{ id: 123 }];
          expectedResult = service.addConsentToCollectionIfMissing(consentCollection, ...consentArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const consent: IConsent = { id: 123 };
          const consent2: IConsent = { id: 456 };
          expectedResult = service.addConsentToCollectionIfMissing([], consent, consent2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(consent);
          expect(expectedResult).toContain(consent2);
        });

        it('should accept null and undefined values', () => {
          const consent: IConsent = { id: 123 };
          expectedResult = service.addConsentToCollectionIfMissing([], null, consent, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(consent);
        });

        it('should return initial array if no Consent is added', () => {
          const consentCollection: IConsent[] = [{ id: 123 }];
          expectedResult = service.addConsentToCollectionIfMissing(consentCollection, undefined, null);
          expect(expectedResult).toEqual(consentCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
