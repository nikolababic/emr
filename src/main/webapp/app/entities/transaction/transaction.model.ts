import * as dayjs from 'dayjs';
import { IUser } from 'app/entities/user/user.model';

export interface ITransaction {
  id?: number;
  amount?: number;
  note?: string | null;
  transactionDate?: dayjs.Dayjs | null;
  patient?: IUser | null;
  administrator?: IUser | null;
}

export class Transaction implements ITransaction {
  constructor(
    public id?: number,
    public amount?: number,
    public note?: string | null,
    public transactionDate?: dayjs.Dayjs | null,
    public patient?: IUser | null,
    public administrator?: IUser | null
  ) {}
}

export function getTransactionIdentifier(transaction: ITransaction): number | undefined {
  return transaction.id;
}
