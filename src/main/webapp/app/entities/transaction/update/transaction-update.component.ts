import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { ITransaction, Transaction } from '../transaction.model';
import { TransactionService } from '../service/transaction.service';
import { IUser, User } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { AccountService } from 'app/core/auth/account.service';
import { AppUserService } from 'app/entities/app-user/service/app-user.service';

@Component({
  selector: 'emr-transaction-update',
  templateUrl: './transaction-update.component.html',
})
export class TransactionUpdateComponent implements OnInit {
  isSaving = false;

  usersSharedCollection: IUser[] = [];
  money = 0;

  editForm = this.fb.group({
    id: [],
    amount: [null, [Validators.required, Validators.max(this.money)]],
    note: [],
    transactionDate: [],
    patient: [],
    administrator: [],
  });

  constructor(
    protected accountService: AccountService,
    protected appUserService: AppUserService,
    protected transactionService: TransactionService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.accountService.getAuthenticationState().subscribe(account => {
      console.warn(account);

      if (account?.id) {
        this.appUserService.find(account.id).subscribe(
          (response: any) => {
            if (response.body.money) {
              this.money = response.body.money;
              this.editForm.get('amount')?.setValidators([Validators.required, Validators.max(this.money)])
              this.editForm.get('amount')?.updateValueAndValidity();
            }
          }
        );
      }
      this.activatedRoute.data.subscribe(({ transaction }) => {
        if (transaction.id === undefined) {
          const today = dayjs().startOf('day');
          transaction.transactionDate = today;
        }

        if (this.activatedRoute.snapshot.params.doctorId) {
          const doctor = new User();
          doctor.id = Number(this.activatedRoute.snapshot.params.doctorId);
          transaction.administrator = doctor;
        }
        const patient = new User();
        patient.id = account?.id;
        transaction.patient = patient;

        this.updateForm(transaction);

        this.loadRelationshipsOptions();
      });
    })

  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const transaction = this.createFromForm();
    if (transaction.id !== undefined) {
      this.subscribeToSaveResponse(this.transactionService.update(transaction));
    } else {
      this.subscribeToSaveResponse(this.transactionService.create(transaction));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITransaction>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(transaction: ITransaction): void {
    this.editForm.patchValue({
      id: transaction.id,
      amount: transaction.amount,
      note: transaction.note,
      transactionDate: transaction.transactionDate ? transaction.transactionDate.format(DATE_TIME_FORMAT) : null,
      patient: transaction.patient,
      administrator: transaction.administrator,
    });

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(
      this.usersSharedCollection,
      transaction.patient,
      transaction.administrator
    );
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(
        map((users: IUser[]) =>
          this.userService.addUserToCollectionIfMissing(
            users,
            this.editForm.get('patient')!.value,
            this.editForm.get('administrator')!.value
          )
        )
      )
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));
  }

  protected createFromForm(): ITransaction {
    return {
      ...new Transaction(),
      id: this.editForm.get(['id'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      note: this.editForm.get(['note'])!.value,
      transactionDate: this.editForm.get(['transactionDate'])!.value
        ? dayjs(this.editForm.get(['transactionDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      patient: this.editForm.get(['patient'])!.value,
      administrator: this.editForm.get(['administrator'])!.value,
    };
  }
}
