import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IAppointment, Appointment } from '../appointment.model';
import { AppointmentService } from '../service/appointment.service';
import { IUser, User } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { AccountService } from 'app/core/auth/account.service';
import { ConsentService } from 'app/entities/consent/service/consent.service';
import { Consent } from 'app/entities/consent/consent.model';

@Component({
  selector: 'emr-appointment-update',
  templateUrl: './appointment-update.component.html',
})
export class AppointmentUpdateComponent implements OnInit {
  isSaving = false;

  usersSharedCollection: IUser[] = [];
  isDoctor: boolean;

  editForm = this.fb.group({
    id: [],
    appointmentDate: [null, Validators.required],
    timeFrom: [null, [Validators.pattern('^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')]],
    timeTo: [null, [Validators.pattern('^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')]],
    appointmentStatus: [],
    patient: [],
    doctor: [],
  });

  constructor(
    protected accountService: AccountService,
    protected consentService: ConsentService,
    protected appointmentService: AppointmentService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.accountService.getAuthenticationState().subscribe(account => {
      this.isDoctor = account?.authorities.includes('ROLE_DOCTOR') ?? false;
      this.activatedRoute.data.subscribe(({ appointment }) => {
        if (appointment.id === undefined) {
          const today = dayjs().startOf('day');
          appointment.appointmentDate = today;
        }
        if (this.activatedRoute.snapshot.params.doctorId) {
          const doctor = new User();
          doctor.id = Number(this.activatedRoute.snapshot.params.doctorId);
          appointment.doctor = doctor;
          appointment.appointmentStatus = 'PENDING'
        } else {
          if (this.isDoctor) {
            const doctor = new User();
            doctor.id = account?.id;
            appointment.doctor = doctor;
          }
        }
        if (!this.isDoctor) {
          const patient = new User();
          patient.id = account?.id;
          appointment.patient = patient;
          appointment.appointmentStatus = 'PENDING'
        }
        this.updateForm(appointment);

        this.loadRelationshipsOptions();
      });
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const appointment = this.createFromForm();
    if (appointment.id !== undefined) {
      this.subscribeToSaveResponse(this.appointmentService.update(appointment));
    } else {
      const consent = new Consent();
      consent.consentStatus = true;
      consent.doctor = appointment.doctor;
      consent.patient = appointment.patient;
      this.consentService.create(consent).subscribe();
      this.subscribeToSaveResponse(this.appointmentService.create(appointment));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAppointment>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(appointment: IAppointment): void {
    this.editForm.patchValue({
      id: appointment.id,
      appointmentDate: appointment.appointmentDate ? appointment.appointmentDate.format(DATE_TIME_FORMAT) : null,
      timeFrom: appointment.timeFrom,
      timeTo: appointment.timeTo,
      appointmentStatus: appointment.appointmentStatus,
      patient: appointment.patient,
      doctor: appointment.doctor,
    });

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(
      this.usersSharedCollection,
      appointment.patient,
      appointment.doctor
    );
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(
        map((users: IUser[]) =>
          this.userService.addUserToCollectionIfMissing(users, this.editForm.get('patient')!.value, this.editForm.get('doctor')!.value)
        )
      )
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));
  }

  protected createFromForm(): IAppointment {
    return {
      ...new Appointment(),
      id: this.editForm.get(['id'])!.value,
      appointmentDate: this.editForm.get(['appointmentDate'])!.value
        ? dayjs(this.editForm.get(['appointmentDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      timeFrom: this.editForm.get(['timeFrom'])!.value,
      timeTo: this.editForm.get(['timeTo'])!.value,
      appointmentStatus: this.editForm.get(['appointmentStatus'])!.value,
      patient: this.editForm.get(['patient'])!.value,
      doctor: this.editForm.get(['doctor'])!.value,
    };
  }
}
