import * as dayjs from 'dayjs';
import { IUser } from 'app/entities/user/user.model';
import { AppointmentStatus } from 'app/entities/enumerations/appointment-status.model';

export interface IAppointment {
  id?: number;
  appointmentDate?: dayjs.Dayjs | null;
  timeFrom?: string | null;
  timeTo?: string | null;
  appointmentStatus?: AppointmentStatus | null;
  patient?: IUser | null;
  doctor?: IUser | null;
}

export class Appointment implements IAppointment {
  constructor(
    public id?: number,
    public appointmentDate?: dayjs.Dayjs | null,
    public timeFrom?: string | null,
    public timeTo?: string | null,
    public appointmentStatus?: AppointmentStatus | null,
    public patient?: IUser | null,
    public doctor?: IUser | null
  ) {}
}

export function getAppointmentIdentifier(appointment: IAppointment): number | undefined {
  return appointment.id;
}
