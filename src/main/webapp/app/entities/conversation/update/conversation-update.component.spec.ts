jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ConversationService } from '../service/conversation.service';
import { IConversation, Conversation } from '../conversation.model';

import { ConversationUpdateComponent } from './conversation-update.component';

describe('Component Tests', () => {
  describe('Conversation Management Update Component', () => {
    let comp: ConversationUpdateComponent;
    let fixture: ComponentFixture<ConversationUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let conversationService: ConversationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ConversationUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ConversationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ConversationUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      conversationService = TestBed.inject(ConversationService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const conversation: IConversation = { id: 456 };

        activatedRoute.data = of({ conversation });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(conversation));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Conversation>>();
        const conversation = { id: 123 };
        jest.spyOn(conversationService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ conversation });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: conversation }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(conversationService.update).toHaveBeenCalledWith(conversation);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Conversation>>();
        const conversation = new Conversation();
        jest.spyOn(conversationService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ conversation });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: conversation }));
        saveSubject.complete();

        // THEN
        expect(conversationService.create).toHaveBeenCalledWith(conversation);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Conversation>>();
        const conversation = { id: 123 };
        jest.spyOn(conversationService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ conversation });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(conversationService.update).toHaveBeenCalledWith(conversation);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
