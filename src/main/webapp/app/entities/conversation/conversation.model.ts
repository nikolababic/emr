export interface IConversation {
  id?: number;
  name?: string | null;
}

export class Conversation implements IConversation {
  constructor(public id?: number, public name?: string | null) {}
}

export function getConversationIdentifier(conversation: IConversation): number | undefined {
  return conversation.id;
}
