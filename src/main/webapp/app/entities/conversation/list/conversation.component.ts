import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Conversation, IConversation } from '../conversation.model';

import { ASC, DESC, ITEMS_PER_PAGE, SORT } from 'app/config/pagination.constants';
import { ConversationService } from '../service/conversation.service';
import { ConversationDeleteDialogComponent } from '../delete/conversation-delete-dialog.component';
import { IResponseList } from 'app/shared/model/response-list.model';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { IMessage } from 'app/entities/message/message.model';
import { ParticipantsService } from 'app/entities/participants/service/participants.service';
import { Participants } from 'app/entities/participants/participants.model';
import { User } from 'app/admin/user-management/user-management.model';
@Component({
  selector: 'emr-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./convesation.component.scss'],
})
export class ConversationComponent implements OnInit {
  @ViewChild('chatHistory') chatHistory: ElementRef;
  conversations?: any[];
  messages?: any[] = [];
  message = '';
  isLoading = false;
  activeConversation: any = null;
  user: Account | null = null;
  isDoctor = false;

  constructor(
    protected conversationService: ConversationService,
    protected activatedRoute: ActivatedRoute,
    private accountService: AccountService,
    private participantService: ParticipantsService,
    protected router: Router,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.accountService.getAuthenticationState().subscribe(account => {
      this.user = account ?? null;
      this.isDoctor = account?.authorities.includes('ROLE_DOCTOR') ?? false;
      this.handleNavigation();
    });
  }

  loadPage(withCreating: boolean): void {
    this.isLoading = true;
    this.conversationService.getMyConvesations().subscribe(
      (res: HttpResponse<IResponseList>) => {
        this.isLoading = false;
        if (res.body?.status) {
          this.onSuccess(res.body.list);

          if (this.conversations?.length) {
            this.setActiveConvesation(this.conversations[0]);
          }
          if (!this.isDoctor && withCreating && this.user !== null) {
            const found = this.conversations?.find(e => e.name === String(this.activatedRoute.snapshot.params.docId) + '-' + String(this.user?.id))
            if (!found && this.activatedRoute.snapshot.params.docId) {
              this.createConversation(this.activatedRoute.snapshot.params.docId);
            }
            if (found) {
              this.setActiveConvesation(found);
            }
          }

        } else {
          this.onError();
        }
      },
      () => {
        this.isLoading = false;
        this.onError();
      }
    );
  }

  setActiveConvesation(convesation: any): void {
    if (this.activeConversation !== convesation) {
      this.activeConversation = convesation;
      this.getMessages();
    }
  }

  getMessages(): void {
    this.conversationService.getMessages(this.activeConversation.id).subscribe(
      (res: HttpResponse<IResponseList>) => {
        if (res.body?.status) {
          this.messages = res.body.list ?? [];
          this.scrollToBottom();
        }
      },
      err => {
        console.error(err);
      }
    );
  }

  delete(conversation: IConversation): void {
    const modalRef = this.modalService.open(ConversationDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.conversation = conversation;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadPage(false);
      }
    });
  }

  sendMessage(): void {
    const message = {
      message: this.message,
      conversationId: this.activeConversation.id,
      userId: this.user?.id,
    };
    this.conversationService.sendMessage(message).subscribe(res => {
      this.message = '';
      this.getMessages();
    });
  }

  createConversation(doctorId: number): void {
    if (this.user !== null) {
      const conversation: IConversation = { name: (String(doctorId) + '-' + String(this.user.id)) };
      this.conversationService.create(conversation).subscribe(res => {
        if (res.body !== null && this.user !== null) {
          conversation.id = res.body.id;

          const participant1 = new Participants();
          participant1.conversation = res.body;
          participant1.user = new User(this.user.id);
          participant1.seen = false;

          const participant2 = new Participants();
          participant2.conversation = res.body;
          participant2.user = new User(doctorId);
          participant2.seen = false;
          this.participantService.createMultiple(participant1, participant2).subscribe(responseList => {
            this.loadPage(false);
          });
        }
      });
    }
  }

  scrollToBottom(): void {
    try {
      setTimeout(() => {
        this.chatHistory.nativeElement.scrollTop = this.chatHistory.nativeElement.scrollHeight;
      }, 0);
    } catch (err) {
      console.warn(err);
    }
  }

  protected handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap]).subscribe(([data, params]) => {
      // const page = params.get('page');


      // const pageNumber = +(page ?? 1);
      this.loadPage(!!this.activatedRoute.snapshot.params.docId);
    });
  }

  protected onSuccess(data: any): void {
    this.conversations = data ?? [];
  }

  protected onError(): void {
    // this.ngbPaginationPage = this.page ?? 1;
  }
}
