import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';

export type EntityArrayResponseType = HttpResponse<string[]>;


@Injectable({ providedIn: 'root' })
export class MapService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/record-types');
  protected specializationsUrl = this.applicationConfigService.getEndpointFor('api/specializations');
  protected doctorsUrl = this.applicationConfigService.getEndpointFor('api/doctors-with-institutions');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}


  getSpecializations(): Observable<any> {
    return this.http
      .get<string[]>(`${this.specializationsUrl}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => res.body));
  }

  getDoctors(): Observable<any> {
    return this.http
      .get<string[]>(`${this.doctorsUrl}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => res.body));
  }
}
