import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MapComponent } from '../list/map.component';

const mapRoute: Routes = [
  {
    path: '',
    component: MapComponent,
    canActivate: [UserRouteAccessService],
  }
];

@NgModule({
  imports: [RouterModule.forChild(mapRoute)],
  exports: [RouterModule],
})
export class MapRoutingModule {}
