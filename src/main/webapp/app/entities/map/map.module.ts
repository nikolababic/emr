import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MapComponent } from './list/map.component';
import { MapRoutingModule } from './route/map-routing.module';

@NgModule({
  imports: [SharedModule, MapRoutingModule],
  declarations: [MapComponent],
  entryComponents: [],
})
export class MapModule {}
