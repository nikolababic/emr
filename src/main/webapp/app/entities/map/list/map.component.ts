import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { MapService } from '../service/map.service';
import {} from 'googlemaps';

@Component({
  selector: 'emr-map',
  templateUrl: './map.component.html',
})
export class MapComponent implements OnInit {
  @ViewChild('map', { static: true }) mapElement: any;
  map!: google.maps.Map;
  marker: any = null;

  isLoading = false;
  specializations: string[] = [];
  doctors: any[] = [];
  filteredDoctors: any[] = [];

  selectedDoctor: any;
  activeSpecialization: string;
  searchTerm: string;

  constructor(
    protected mapService: MapService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.initData();
    const mapProperties = {
      center: new google.maps.LatLng(44.01667, 20.91667),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
  }

  initData(): void {
    this.mapService.getSpecializations().subscribe(data => {
      this.specializations = data;
    });
    this.mapService.getDoctors().subscribe(data => {
      this.doctors = data;
      this.filteredDoctors = data;
    });
  }

  searchDoctor(): void {
    let tmpArray = this.doctors;

    if (this.searchTerm) {
      tmpArray = this.doctors.filter(
        doctor =>
          doctor.first_name.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
          doctor.last_name.toLowerCase().includes(this.searchTerm.toLowerCase())
      );
    } else {
      tmpArray = this.doctors;
    }
    if (!!this.activeSpecialization  && this.activeSpecialization !== '') {
      tmpArray = tmpArray.filter(doctor => doctor.specialization === this.activeSpecialization);
    } 
    this.filteredDoctors = tmpArray;
  }

  selectDoctor(doctor: any): void {
    this.selectedDoctor = doctor;
    if (doctor.latitude && doctor.longitude) {
      const mapProperties = {
        center: new google.maps.LatLng(doctor.latitude, doctor.longitude),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      };
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);

      this.marker = new google.maps.Marker({
        position: { lat: doctor.latitude, lng: doctor.longitude },
        map: this.map,
        title: 'markers',
        label: doctor.name || 'Ordinacija',
      });
    }
  }
}
