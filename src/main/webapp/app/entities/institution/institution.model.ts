import { IAddress } from 'app/entities/address/address.model';

export interface IInstitution {
  id?: number;
  name?: string;
  phoneNumber?: string | null;
  address?: IAddress | null;
}

export class Institution implements IInstitution {
  constructor(public id?: number, public name?: string, public phoneNumber?: string | null, public address?: IAddress | null) {}
}

export function getInstitutionIdentifier(institution: IInstitution): number | undefined {
  return institution.id;
}
