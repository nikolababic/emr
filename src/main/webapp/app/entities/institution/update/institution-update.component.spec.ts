jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { InstitutionService } from '../service/institution.service';
import { IInstitution, Institution } from '../institution.model';
import { IAddress } from 'app/entities/address/address.model';
import { AddressService } from 'app/entities/address/service/address.service';

import { InstitutionUpdateComponent } from './institution-update.component';

describe('Component Tests', () => {
  describe('Institution Management Update Component', () => {
    let comp: InstitutionUpdateComponent;
    let fixture: ComponentFixture<InstitutionUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let institutionService: InstitutionService;
    let addressService: AddressService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [InstitutionUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(InstitutionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(InstitutionUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      institutionService = TestBed.inject(InstitutionService);
      addressService = TestBed.inject(AddressService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Address query and add missing value', () => {
        const institution: IInstitution = { id: 456 };
        const address: IAddress = { id: 14955 };
        institution.address = address;

        const addressCollection: IAddress[] = [{ id: 729 }];
        jest.spyOn(addressService, 'query').mockReturnValue(of(new HttpResponse({ body: addressCollection })));
        const additionalAddresses = [address];
        const expectedCollection: IAddress[] = [...additionalAddresses, ...addressCollection];
        jest.spyOn(addressService, 'addAddressToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ institution });
        comp.ngOnInit();

        expect(addressService.query).toHaveBeenCalled();
        expect(addressService.addAddressToCollectionIfMissing).toHaveBeenCalledWith(addressCollection, ...additionalAddresses);
        expect(comp.addressesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const institution: IInstitution = { id: 456 };
        const address: IAddress = { id: 12444 };
        institution.address = address;

        activatedRoute.data = of({ institution });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(institution));
        expect(comp.addressesSharedCollection).toContain(address);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Institution>>();
        const institution = { id: 123 };
        jest.spyOn(institutionService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ institution });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: institution }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(institutionService.update).toHaveBeenCalledWith(institution);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Institution>>();
        const institution = new Institution();
        jest.spyOn(institutionService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ institution });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: institution }));
        saveSubject.complete();

        // THEN
        expect(institutionService.create).toHaveBeenCalledWith(institution);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Institution>>();
        const institution = { id: 123 };
        jest.spyOn(institutionService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ institution });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(institutionService.update).toHaveBeenCalledWith(institution);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackAddressById', () => {
        it('Should return tracked Address primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackAddressById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
