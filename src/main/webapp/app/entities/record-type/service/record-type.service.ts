import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IRecordType, getRecordTypeIdentifier } from '../record-type.model';

export type EntityResponseType = HttpResponse<IRecordType>;
export type EntityArrayResponseType = HttpResponse<IRecordType[]>;

@Injectable({ providedIn: 'root' })
export class RecordTypeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/record-types');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(recordType: IRecordType): Observable<EntityResponseType> {
    return this.http.post<IRecordType>(this.resourceUrl, recordType, { observe: 'response' });
  }

  update(recordType: IRecordType): Observable<EntityResponseType> {
    return this.http.put<IRecordType>(`${this.resourceUrl}/${getRecordTypeIdentifier(recordType) as number}`, recordType, {
      observe: 'response',
    });
  }

  partialUpdate(recordType: IRecordType): Observable<EntityResponseType> {
    return this.http.patch<IRecordType>(`${this.resourceUrl}/${getRecordTypeIdentifier(recordType) as number}`, recordType, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRecordType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRecordType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addRecordTypeToCollectionIfMissing(
    recordTypeCollection: IRecordType[],
    ...recordTypesToCheck: (IRecordType | null | undefined)[]
  ): IRecordType[] {
    const recordTypes: IRecordType[] = recordTypesToCheck.filter(isPresent);
    if (recordTypes.length > 0) {
      const recordTypeCollectionIdentifiers = recordTypeCollection.map(recordTypeItem => getRecordTypeIdentifier(recordTypeItem)!);
      const recordTypesToAdd = recordTypes.filter(recordTypeItem => {
        const recordTypeIdentifier = getRecordTypeIdentifier(recordTypeItem);
        if (recordTypeIdentifier == null || recordTypeCollectionIdentifiers.includes(recordTypeIdentifier)) {
          return false;
        }
        recordTypeCollectionIdentifiers.push(recordTypeIdentifier);
        return true;
      });
      return [...recordTypesToAdd, ...recordTypeCollection];
    }
    return recordTypeCollection;
  }
}
