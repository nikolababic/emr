import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IRecordType, RecordType } from '../record-type.model';

import { RecordTypeService } from './record-type.service';

describe('Service Tests', () => {
  describe('RecordType Service', () => {
    let service: RecordTypeService;
    let httpMock: HttpTestingController;
    let elemDefault: IRecordType;
    let expectedResult: IRecordType | IRecordType[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(RecordTypeService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        name: 'AAAAAAA',
        visibleColumns: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a RecordType', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new RecordType()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a RecordType', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            visibleColumns: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a RecordType', () => {
        const patchObject = Object.assign(
          {
            name: 'BBBBBB',
            visibleColumns: 'BBBBBB',
          },
          new RecordType()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of RecordType', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            visibleColumns: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a RecordType', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addRecordTypeToCollectionIfMissing', () => {
        it('should add a RecordType to an empty array', () => {
          const recordType: IRecordType = { id: 123 };
          expectedResult = service.addRecordTypeToCollectionIfMissing([], recordType);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(recordType);
        });

        it('should not add a RecordType to an array that contains it', () => {
          const recordType: IRecordType = { id: 123 };
          const recordTypeCollection: IRecordType[] = [
            {
              ...recordType,
            },
            { id: 456 },
          ];
          expectedResult = service.addRecordTypeToCollectionIfMissing(recordTypeCollection, recordType);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a RecordType to an array that doesn't contain it", () => {
          const recordType: IRecordType = { id: 123 };
          const recordTypeCollection: IRecordType[] = [{ id: 456 }];
          expectedResult = service.addRecordTypeToCollectionIfMissing(recordTypeCollection, recordType);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(recordType);
        });

        it('should add only unique RecordType to an array', () => {
          const recordTypeArray: IRecordType[] = [{ id: 123 }, { id: 456 }, { id: 11445 }];
          const recordTypeCollection: IRecordType[] = [{ id: 123 }];
          expectedResult = service.addRecordTypeToCollectionIfMissing(recordTypeCollection, ...recordTypeArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const recordType: IRecordType = { id: 123 };
          const recordType2: IRecordType = { id: 456 };
          expectedResult = service.addRecordTypeToCollectionIfMissing([], recordType, recordType2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(recordType);
          expect(expectedResult).toContain(recordType2);
        });

        it('should accept null and undefined values', () => {
          const recordType: IRecordType = { id: 123 };
          expectedResult = service.addRecordTypeToCollectionIfMissing([], null, recordType, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(recordType);
        });

        it('should return initial array if no RecordType is added', () => {
          const recordTypeCollection: IRecordType[] = [{ id: 123 }];
          expectedResult = service.addRecordTypeToCollectionIfMissing(recordTypeCollection, undefined, null);
          expect(expectedResult).toEqual(recordTypeCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
