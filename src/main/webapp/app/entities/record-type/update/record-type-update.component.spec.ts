jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { RecordTypeService } from '../service/record-type.service';
import { IRecordType, RecordType } from '../record-type.model';

import { RecordTypeUpdateComponent } from './record-type-update.component';

describe('Component Tests', () => {
  describe('RecordType Management Update Component', () => {
    let comp: RecordTypeUpdateComponent;
    let fixture: ComponentFixture<RecordTypeUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let recordTypeService: RecordTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [RecordTypeUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(RecordTypeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RecordTypeUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      recordTypeService = TestBed.inject(RecordTypeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const recordType: IRecordType = { id: 456 };

        activatedRoute.data = of({ recordType });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(recordType));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<RecordType>>();
        const recordType = { id: 123 };
        jest.spyOn(recordTypeService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ recordType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: recordType }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(recordTypeService.update).toHaveBeenCalledWith(recordType);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<RecordType>>();
        const recordType = new RecordType();
        jest.spyOn(recordTypeService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ recordType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: recordType }));
        saveSubject.complete();

        // THEN
        expect(recordTypeService.create).toHaveBeenCalledWith(recordType);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<RecordType>>();
        const recordType = { id: 123 };
        jest.spyOn(recordTypeService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ recordType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(recordTypeService.update).toHaveBeenCalledWith(recordType);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
