import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IRecordType, RecordType } from '../record-type.model';
import { RecordTypeService } from '../service/record-type.service';

@Component({
  selector: 'emr-record-type-update',
  templateUrl: './record-type-update.component.html',
})
export class RecordTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(63)]],
    visibleColumns: [],
  });

  constructor(protected recordTypeService: RecordTypeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recordType }) => {
      this.updateForm(recordType);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const recordType = this.createFromForm();
    if (recordType.id !== undefined) {
      this.subscribeToSaveResponse(this.recordTypeService.update(recordType));
    } else {
      this.subscribeToSaveResponse(this.recordTypeService.create(recordType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRecordType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(recordType: IRecordType): void {
    this.editForm.patchValue({
      id: recordType.id,
      name: recordType.name,
      visibleColumns: recordType.visibleColumns,
    });
  }

  protected createFromForm(): IRecordType {
    return {
      ...new RecordType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      visibleColumns: this.editForm.get(['visibleColumns'])!.value,
    };
  }
}
