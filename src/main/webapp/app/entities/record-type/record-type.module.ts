import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { RecordTypeComponent } from './list/record-type.component';
import { RecordTypeDetailComponent } from './detail/record-type-detail.component';
import { RecordTypeUpdateComponent } from './update/record-type-update.component';
import { RecordTypeDeleteDialogComponent } from './delete/record-type-delete-dialog.component';
import { RecordTypeRoutingModule } from './route/record-type-routing.module';

@NgModule({
  imports: [SharedModule, RecordTypeRoutingModule],
  declarations: [RecordTypeComponent, RecordTypeDetailComponent, RecordTypeUpdateComponent, RecordTypeDeleteDialogComponent],
  entryComponents: [RecordTypeDeleteDialogComponent],
})
export class RecordTypeModule {}
