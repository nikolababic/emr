export interface IRecordType {
  id?: number;
  name?: string;
  visibleColumns?: string | null;
}

export class RecordType implements IRecordType {
  constructor(public id?: number, public name?: string, public visibleColumns?: string | null) {}
}

export function getRecordTypeIdentifier(recordType: IRecordType): number | undefined {
  return recordType.id;
}
