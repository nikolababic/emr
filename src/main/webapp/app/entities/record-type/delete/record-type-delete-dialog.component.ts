import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IRecordType } from '../record-type.model';
import { RecordTypeService } from '../service/record-type.service';

@Component({
  templateUrl: './record-type-delete-dialog.component.html',
})
export class RecordTypeDeleteDialogComponent {
  recordType?: IRecordType;

  constructor(protected recordTypeService: RecordTypeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.recordTypeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
