import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IRecordType, RecordType } from '../record-type.model';
import { RecordTypeService } from '../service/record-type.service';

@Injectable({ providedIn: 'root' })
export class RecordTypeRoutingResolveService implements Resolve<IRecordType> {
  constructor(protected service: RecordTypeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRecordType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((recordType: HttpResponse<RecordType>) => {
          if (recordType.body) {
            return of(recordType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RecordType());
  }
}
