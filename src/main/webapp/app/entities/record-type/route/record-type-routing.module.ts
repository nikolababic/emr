import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { RecordTypeComponent } from '../list/record-type.component';
import { RecordTypeDetailComponent } from '../detail/record-type-detail.component';
import { RecordTypeUpdateComponent } from '../update/record-type-update.component';
import { RecordTypeRoutingResolveService } from './record-type-routing-resolve.service';

const recordTypeRoute: Routes = [
  {
    path: '',
    component: RecordTypeComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RecordTypeDetailComponent,
    resolve: {
      recordType: RecordTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RecordTypeUpdateComponent,
    resolve: {
      recordType: RecordTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RecordTypeUpdateComponent,
    resolve: {
      recordType: RecordTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(recordTypeRoute)],
  exports: [RouterModule],
})
export class RecordTypeRoutingModule {}
