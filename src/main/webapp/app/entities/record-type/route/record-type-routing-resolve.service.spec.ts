jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IRecordType, RecordType } from '../record-type.model';
import { RecordTypeService } from '../service/record-type.service';

import { RecordTypeRoutingResolveService } from './record-type-routing-resolve.service';

describe('Service Tests', () => {
  describe('RecordType routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: RecordTypeRoutingResolveService;
    let service: RecordTypeService;
    let resultRecordType: IRecordType | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(RecordTypeRoutingResolveService);
      service = TestBed.inject(RecordTypeService);
      resultRecordType = undefined;
    });

    describe('resolve', () => {
      it('should return IRecordType returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultRecordType = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultRecordType).toEqual({ id: 123 });
      });

      it('should return new IRecordType if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultRecordType = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultRecordType).toEqual(new RecordType());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as RecordType })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultRecordType = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultRecordType).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
