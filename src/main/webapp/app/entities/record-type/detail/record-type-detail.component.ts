import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRecordType } from '../record-type.model';

@Component({
  selector: 'emr-record-type-detail',
  templateUrl: './record-type-detail.component.html',
})
export class RecordTypeDetailComponent implements OnInit {
  recordType: IRecordType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recordType }) => {
      this.recordType = recordType;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
