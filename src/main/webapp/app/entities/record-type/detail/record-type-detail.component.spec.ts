import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecordTypeDetailComponent } from './record-type-detail.component';

describe('Component Tests', () => {
  describe('RecordType Management Detail Component', () => {
    let comp: RecordTypeDetailComponent;
    let fixture: ComponentFixture<RecordTypeDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [RecordTypeDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ recordType: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(RecordTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RecordTypeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load recordType on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.recordType).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
