export interface IAddress {
  id?: number;
  street?: string | null;
  city?: string | null;
  latitude?: number | null;
  longitude?: number | null;
}

export class Address implements IAddress {
  constructor(
    public id?: number,
    public street?: string | null,
    public city?: string | null,
    public latitude?: number | null,
    public longitude?: number | null
  ) {}
}

export function getAddressIdentifier(address: IAddress): number | undefined {
  return address.id;
}
