import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'app-user',
        data: { pageTitle: 'emrApp.appUser.home.title' },
        loadChildren: () => import('./app-user/app-user.module').then(m => m.AppUserModule),
      },
      {
        path: 'address',
        data: { pageTitle: 'emrApp.address.home.title' },
        loadChildren: () => import('./address/address.module').then(m => m.AddressModule),
      },
      {
        path: 'institution',
        data: { pageTitle: 'emrApp.institution.home.title' },
        loadChildren: () => import('./institution/institution.module').then(m => m.InstitutionModule),
      },
      {
        path: 'transaction',
        data: { pageTitle: 'emrApp.transaction.home.title' },
        loadChildren: () => import('./transaction/transaction.module').then(m => m.TransactionModule),
      },
      {
        path: 'record-type',
        data: { pageTitle: 'emrApp.recordType.home.title' },
        loadChildren: () => import('./record-type/record-type.module').then(m => m.RecordTypeModule),
      },
      {
        path: 'record',
        data: { pageTitle: 'emrApp.record.home.title' },
        loadChildren: () => import('./record/record.module').then(m => m.RecordModule),
      },
      {
        path: 'document',
        data: { pageTitle: 'emrApp.document.home.title' },
        loadChildren: () => import('./document/document.module').then(m => m.DocumentModule),
      },
      {
        path: 'conversation',
        data: { pageTitle: 'emrApp.conversation.home.title' },
        loadChildren: () => import('./conversation/conversation.module').then(m => m.ConversationModule),
      },
      {
        path: 'participants',
        data: { pageTitle: 'emrApp.participants.home.title' },
        loadChildren: () => import('./participants/participants.module').then(m => m.ParticipantsModule),
      },
      {
        path: 'message',
        data: { pageTitle: 'emrApp.message.home.title' },
        loadChildren: () => import('./message/message.module').then(m => m.MessageModule),
      },
      {
        path: 'my-patients',
        data: { pageTitle: 'emrApp.consent.home.title' },
        loadChildren: () => import('./consent/consent.module').then(m => m.ConsentModule),
      },
      {
        path: 'work-hour',
        data: { pageTitle: 'emrApp.workHour.home.title' },
        loadChildren: () => import('./work-hour/work-hour.module').then(m => m.WorkHourModule),
      },
      {
        path: 'appointment',
        data: { pageTitle: 'emrApp.appointment.home.title' },
        loadChildren: () => import('./appointment/appointment.module').then(m => m.AppointmentModule),
      },
      {
        path: 'search-doctor',
        data: { pageTitle: 'emrApp.document.home.title' },
        loadChildren: () => import('./map/map.module').then(m => m.MapModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
