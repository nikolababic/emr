import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IRecord, Record } from '../record.model';
import { RecordService } from '../service/record.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { IRecordType } from 'app/entities/record-type/record-type.model';
import { RecordTypeService } from 'app/entities/record-type/service/record-type.service';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'emr-record-update',
  templateUrl: './record-update.component.html',
})
export class RecordUpdateComponent implements OnInit {
  isSaving = false;

  userId?: string;
  recordTypesSharedCollection: IRecordType[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    recordDate: [],
    additionalInformation: [],
    active: [],
    user: [],
    recordType: [],
  });

  constructor(
    protected recordService: RecordService,
    protected userService: UserService,
    private accountService: AccountService,
    protected recordTypeService: RecordTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ record }) => {
      if (record.id === undefined) {
        const today = dayjs().startOf('day');
        record.recordDate = today;
      }
      this.userId = this.activatedRoute.snapshot.paramMap.get('user') ?? '';
      if (this.userId === 'me') {
        this.accountService.getAuthenticationState().subscribe(account => (this.userId = String(account?.id)));
      }

      this.updateForm(record);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const record = this.createFromForm();
    if (!record.id) {
      this.subscribeToSaveResponse(this.recordService.create(record));
    } else {
      this.subscribeToSaveResponse(this.recordService.update(record));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  trackRecordTypeById(index: number, item: IRecordType): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRecord>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(record: IRecord): void {
    this.editForm.patchValue({
      id: record.id,
      name: record.name,
      recordDate: record.recordDate ? record.recordDate.format(DATE_TIME_FORMAT) : null,
      additionalInformation: record.additionalInformation,
      active: record.active,
      user: record.user,
      recordType: record.recordType,
    });

    this.recordTypesSharedCollection = this.recordTypeService.addRecordTypeToCollectionIfMissing(
      this.recordTypesSharedCollection,
      record.recordType
    );
  }

  protected loadRelationshipsOptions(): void {
    this.recordTypeService
      .query()
      .pipe(map((res: HttpResponse<IRecordType[]>) => res.body ?? []))
      .pipe(
        map((recordTypes: IRecordType[]) =>
          this.recordTypeService.addRecordTypeToCollectionIfMissing(recordTypes, this.editForm.get('recordType')!.value)
        )
      )
      .subscribe((recordTypes: IRecordType[]) => (this.recordTypesSharedCollection = recordTypes));
  }

  protected createFromForm(): IRecord {
    return {
      ...new Record(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      recordDate: this.editForm.get(['recordDate'])!.value ? dayjs(this.editForm.get(['recordDate'])!.value, DATE_TIME_FORMAT) : undefined,
      additionalInformation: this.editForm.get(['additionalInformation'])!.value,
      active: this.editForm.get(['active'])!.value,
      user: { id: Number(this.userId) },
      recordType: this.editForm.get(['recordType'])!.value,
    };
  }
}
