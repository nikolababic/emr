jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { RecordService } from '../service/record.service';
import { IRecord, Record } from '../record.model';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { IRecordType } from 'app/entities/record-type/record-type.model';
import { RecordTypeService } from 'app/entities/record-type/service/record-type.service';

import { RecordUpdateComponent } from './record-update.component';

describe('Component Tests', () => {
  describe('Record Management Update Component', () => {
    let comp: RecordUpdateComponent;
    let fixture: ComponentFixture<RecordUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let recordService: RecordService;
    let userService: UserService;
    let recordTypeService: RecordTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [RecordUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(RecordUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RecordUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      recordService = TestBed.inject(RecordService);
      userService = TestBed.inject(UserService);
      recordTypeService = TestBed.inject(RecordTypeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call User query and add missing value', () => {
        const record: IRecord = { id: 456 };
        const user: IUser = { id: 40829 };
        record.user = user;

        const userCollection: IUser[] = [{ id: 799 }];
        jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
        const additionalUsers = [user];
        const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
        jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ record });
        comp.ngOnInit();

        expect(userService.query).toHaveBeenCalled();
        expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(userCollection, ...additionalUsers);
        expect(comp.usersSharedCollection).toEqual(expectedCollection);
      });

      it('Should call RecordType query and add missing value', () => {
        const record: IRecord = { id: 456 };
        const recordType: IRecordType = { id: 85262 };
        record.recordType = recordType;

        const recordTypeCollection: IRecordType[] = [{ id: 17789 }];
        jest.spyOn(recordTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: recordTypeCollection })));
        const additionalRecordTypes = [recordType];
        const expectedCollection: IRecordType[] = [...additionalRecordTypes, ...recordTypeCollection];
        jest.spyOn(recordTypeService, 'addRecordTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ record });
        comp.ngOnInit();

        expect(recordTypeService.query).toHaveBeenCalled();
        expect(recordTypeService.addRecordTypeToCollectionIfMissing).toHaveBeenCalledWith(recordTypeCollection, ...additionalRecordTypes);
        expect(comp.recordTypesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const record: IRecord = { id: 456 };
        const user: IUser = { id: 48865 };
        record.user = user;
        const recordType: IRecordType = { id: 42690 };
        record.recordType = recordType;

        activatedRoute.data = of({ record });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(record));
        expect(comp.usersSharedCollection).toContain(user);
        expect(comp.recordTypesSharedCollection).toContain(recordType);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Record>>();
        const record = { id: 123 };
        jest.spyOn(recordService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ record });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: record }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(recordService.update).toHaveBeenCalledWith(record);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Record>>();
        const record = new Record();
        jest.spyOn(recordService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ record });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: record }));
        saveSubject.complete();

        // THEN
        expect(recordService.create).toHaveBeenCalledWith(record);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Record>>();
        const record = { id: 123 };
        jest.spyOn(recordService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ record });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(recordService.update).toHaveBeenCalledWith(record);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackUserById', () => {
        it('Should return tracked User primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackUserById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackRecordTypeById', () => {
        it('Should return tracked RecordType primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackRecordTypeById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
