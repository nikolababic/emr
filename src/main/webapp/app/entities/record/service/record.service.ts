import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IRecord, getRecordIdentifier } from '../record.model';
import { IRecordType } from 'app/entities/record-type/record-type.model';

export type EntityResponseType = HttpResponse<IRecord>;
export type EntityArrayResponseType = HttpResponse<IRecord[]>;
export type EntityTypesArrayResponseType = HttpResponse<IRecordType[]>;

@Injectable({ providedIn: 'root' })
export class RecordService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/records');
  protected resourceTypesUrl = this.applicationConfigService.getEndpointFor('api/record-types');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(record: IRecord): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(record);
    return this.http
      .post<IRecord>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(record: IRecord): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(record);
    return this.http
      .put<IRecord>(`${this.resourceUrl}/${getRecordIdentifier(record) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(record: IRecord): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(record);
    return this.http
      .patch<IRecord>(`${this.resourceUrl}/${getRecordIdentifier(record) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IRecord>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRecord[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  queryTypes(req?: any): Observable<EntityTypesArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRecordType[]>(this.resourceTypesUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addRecordToCollectionIfMissing(recordCollection: IRecord[], ...recordsToCheck: (IRecord | null | undefined)[]): IRecord[] {
    const records: IRecord[] = recordsToCheck.filter(isPresent);
    if (records.length > 0) {
      const recordCollectionIdentifiers = recordCollection.map(recordItem => getRecordIdentifier(recordItem)!);
      const recordsToAdd = records.filter(recordItem => {
        const recordIdentifier = getRecordIdentifier(recordItem);
        if (recordIdentifier == null || recordCollectionIdentifiers.includes(recordIdentifier)) {
          return false;
        }
        recordCollectionIdentifiers.push(recordIdentifier);
        return true;
      });
      return [...recordsToAdd, ...recordCollection];
    }
    return recordCollection;
  }

  protected convertDateFromClient(record: IRecord): IRecord {
    return Object.assign({}, record, {
      recordDate: record.recordDate?.isValid() ? record.recordDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.recordDate = res.body.recordDate ? dayjs(res.body.recordDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((record: IRecord) => {
        record.recordDate = record.recordDate ? dayjs(record.recordDate) : undefined;
      });
    }
    return res;
  }
}
