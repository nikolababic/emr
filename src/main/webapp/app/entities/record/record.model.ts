import * as dayjs from 'dayjs';
import { IUser } from 'app/entities/user/user.model';
import { IRecordType } from 'app/entities/record-type/record-type.model';

export interface IRecord {
  id?: number;
  name?: string | null;
  recordDate?: dayjs.Dayjs | null;
  additionalInformation?: string | null;
  active?: boolean | null;
  user?: IUser | null;
  recordType?: IRecordType | null;
}

export class Record implements IRecord {
  constructor(
    public id?: number,
    public name?: string | null,
    public recordDate?: dayjs.Dayjs | null,
    public additionalInformation?: string | null,
    public active?: boolean | null,
    public user?: IUser | null,
    public recordType?: IRecordType | null
  ) {
    this.active = this.active ?? false;
  }
}

export function getRecordIdentifier(record: IRecord): number | undefined {
  return record.id;
}
