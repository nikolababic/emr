import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IParticipants, Participants } from '../participants.model';
import { ParticipantsService } from '../service/participants.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { IConversation } from 'app/entities/conversation/conversation.model';
import { ConversationService } from 'app/entities/conversation/service/conversation.service';

@Component({
  selector: 'emr-participants-update',
  templateUrl: './participants-update.component.html',
})
export class ParticipantsUpdateComponent implements OnInit {
  isSaving = false;

  usersSharedCollection: IUser[] = [];
  conversationsSharedCollection: IConversation[] = [];

  editForm = this.fb.group({
    id: [],
    seen: [],
    user: [],
    conversation: [],
  });

  constructor(
    protected participantsService: ParticipantsService,
    protected userService: UserService,
    protected conversationService: ConversationService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ participants }) => {
      this.updateForm(participants);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const participants = this.createFromForm();
    if (participants.id !== undefined) {
      this.subscribeToSaveResponse(this.participantsService.update(participants));
    } else {
      this.subscribeToSaveResponse(this.participantsService.create(participants));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  trackConversationById(index: number, item: IConversation): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IParticipants>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(participants: IParticipants): void {
    this.editForm.patchValue({
      id: participants.id,
      seen: participants.seen,
      user: participants.user,
      conversation: participants.conversation,
    });

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(this.usersSharedCollection, participants.user);
    this.conversationsSharedCollection = this.conversationService.addConversationToCollectionIfMissing(
      this.conversationsSharedCollection,
      participants.conversation
    );
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, this.editForm.get('user')!.value)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));

    this.conversationService
      .query()
      .pipe(map((res: HttpResponse<IConversation[]>) => res.body ?? []))
      .pipe(
        map((conversations: IConversation[]) =>
          this.conversationService.addConversationToCollectionIfMissing(conversations, this.editForm.get('conversation')!.value)
        )
      )
      .subscribe((conversations: IConversation[]) => (this.conversationsSharedCollection = conversations));
  }

  protected createFromForm(): IParticipants {
    return {
      ...new Participants(),
      id: this.editForm.get(['id'])!.value,
      seen: this.editForm.get(['seen'])!.value,
      user: this.editForm.get(['user'])!.value,
      conversation: this.editForm.get(['conversation'])!.value,
    };
  }
}
