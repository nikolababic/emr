jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ParticipantsService } from '../service/participants.service';
import { IParticipants, Participants } from '../participants.model';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { IConversation } from 'app/entities/conversation/conversation.model';
import { ConversationService } from 'app/entities/conversation/service/conversation.service';

import { ParticipantsUpdateComponent } from './participants-update.component';

describe('Component Tests', () => {
  describe('Participants Management Update Component', () => {
    let comp: ParticipantsUpdateComponent;
    let fixture: ComponentFixture<ParticipantsUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let participantsService: ParticipantsService;
    let userService: UserService;
    let conversationService: ConversationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ParticipantsUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ParticipantsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ParticipantsUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      participantsService = TestBed.inject(ParticipantsService);
      userService = TestBed.inject(UserService);
      conversationService = TestBed.inject(ConversationService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call User query and add missing value', () => {
        const participants: IParticipants = { id: 456 };
        const user: IUser = { id: 83283 };
        participants.user = user;

        const userCollection: IUser[] = [{ id: 20878 }];
        jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
        const additionalUsers = [user];
        const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
        jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ participants });
        comp.ngOnInit();

        expect(userService.query).toHaveBeenCalled();
        expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(userCollection, ...additionalUsers);
        expect(comp.usersSharedCollection).toEqual(expectedCollection);
      });

      it('Should call Conversation query and add missing value', () => {
        const participants: IParticipants = { id: 456 };
        const conversation: IConversation = { id: 27029 };
        participants.conversation = conversation;

        const conversationCollection: IConversation[] = [{ id: 80870 }];
        jest.spyOn(conversationService, 'query').mockReturnValue(of(new HttpResponse({ body: conversationCollection })));
        const additionalConversations = [conversation];
        const expectedCollection: IConversation[] = [...additionalConversations, ...conversationCollection];
        jest.spyOn(conversationService, 'addConversationToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ participants });
        comp.ngOnInit();

        expect(conversationService.query).toHaveBeenCalled();
        expect(conversationService.addConversationToCollectionIfMissing).toHaveBeenCalledWith(
          conversationCollection,
          ...additionalConversations
        );
        expect(comp.conversationsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const participants: IParticipants = { id: 456 };
        const user: IUser = { id: 57284 };
        participants.user = user;
        const conversation: IConversation = { id: 59388 };
        participants.conversation = conversation;

        activatedRoute.data = of({ participants });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(participants));
        expect(comp.usersSharedCollection).toContain(user);
        expect(comp.conversationsSharedCollection).toContain(conversation);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Participants>>();
        const participants = { id: 123 };
        jest.spyOn(participantsService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ participants });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: participants }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(participantsService.update).toHaveBeenCalledWith(participants);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Participants>>();
        const participants = new Participants();
        jest.spyOn(participantsService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ participants });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: participants }));
        saveSubject.complete();

        // THEN
        expect(participantsService.create).toHaveBeenCalledWith(participants);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Participants>>();
        const participants = { id: 123 };
        jest.spyOn(participantsService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ participants });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(participantsService.update).toHaveBeenCalledWith(participants);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackUserById', () => {
        it('Should return tracked User primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackUserById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackConversationById', () => {
        it('Should return tracked Conversation primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackConversationById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
