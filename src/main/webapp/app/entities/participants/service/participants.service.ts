import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { forkJoin, Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IParticipants, getParticipantsIdentifier } from '../participants.model';

export type EntityResponseType = HttpResponse<IParticipants>;
export type EntityArrayResponseType = HttpResponse<IParticipants[]>;

@Injectable({ providedIn: 'root' })
export class ParticipantsService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/participants');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(participants: IParticipants): Observable<EntityResponseType> {
    return this.http.post<IParticipants>(this.resourceUrl, participants, { observe: 'response' });
  }

  createMultiple(participant1: IParticipants, participant2: IParticipants): Observable<any[]> {
    const response1 = this.http.post<IParticipants>(this.resourceUrl, participant1, { observe: 'response' });
    const response2 = this.http.post<IParticipants>(this.resourceUrl, participant2, { observe: 'response' });
    return forkJoin([response1, response2]);
  }


  update(participants: IParticipants): Observable<EntityResponseType> {
    return this.http.put<IParticipants>(`${this.resourceUrl}/${getParticipantsIdentifier(participants) as number}`, participants, {
      observe: 'response',
    });
  }

  partialUpdate(participants: IParticipants): Observable<EntityResponseType> {
    return this.http.patch<IParticipants>(`${this.resourceUrl}/${getParticipantsIdentifier(participants) as number}`, participants, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IParticipants>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IParticipants[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addParticipantsToCollectionIfMissing(
    participantsCollection: IParticipants[],
    ...participantsToCheck: (IParticipants | null | undefined)[]
  ): IParticipants[] {
    const participants: IParticipants[] = participantsToCheck.filter(isPresent);
    if (participants.length > 0) {
      const participantsCollectionIdentifiers = participantsCollection.map(
        participantsItem => getParticipantsIdentifier(participantsItem)!
      );
      const participantsToAdd = participants.filter(participantsItem => {
        const participantsIdentifier = getParticipantsIdentifier(participantsItem);
        if (participantsIdentifier == null || participantsCollectionIdentifiers.includes(participantsIdentifier)) {
          return false;
        }
        participantsCollectionIdentifiers.push(participantsIdentifier);
        return true;
      });
      return [...participantsToAdd, ...participantsCollection];
    }
    return participantsCollection;
  }
}
