import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IParticipants, Participants } from '../participants.model';

import { ParticipantsService } from './participants.service';

describe('Service Tests', () => {
  describe('Participants Service', () => {
    let service: ParticipantsService;
    let httpMock: HttpTestingController;
    let elemDefault: IParticipants;
    let expectedResult: IParticipants | IParticipants[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ParticipantsService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        seen: false,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Participants', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Participants()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Participants', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            seen: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Participants', () => {
        const patchObject = Object.assign({}, new Participants());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Participants', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            seen: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Participants', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addParticipantsToCollectionIfMissing', () => {
        it('should add a Participants to an empty array', () => {
          const participants: IParticipants = { id: 123 };
          expectedResult = service.addParticipantsToCollectionIfMissing([], participants);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(participants);
        });

        it('should not add a Participants to an array that contains it', () => {
          const participants: IParticipants = { id: 123 };
          const participantsCollection: IParticipants[] = [
            {
              ...participants,
            },
            { id: 456 },
          ];
          expectedResult = service.addParticipantsToCollectionIfMissing(participantsCollection, participants);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Participants to an array that doesn't contain it", () => {
          const participants: IParticipants = { id: 123 };
          const participantsCollection: IParticipants[] = [{ id: 456 }];
          expectedResult = service.addParticipantsToCollectionIfMissing(participantsCollection, participants);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(participants);
        });

        it('should add only unique Participants to an array', () => {
          const participantsArray: IParticipants[] = [{ id: 123 }, { id: 456 }, { id: 77693 }];
          const participantsCollection: IParticipants[] = [{ id: 123 }];
          expectedResult = service.addParticipantsToCollectionIfMissing(participantsCollection, ...participantsArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const participants: IParticipants = { id: 123 };
          const participants2: IParticipants = { id: 456 };
          expectedResult = service.addParticipantsToCollectionIfMissing([], participants, participants2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(participants);
          expect(expectedResult).toContain(participants2);
        });

        it('should accept null and undefined values', () => {
          const participants: IParticipants = { id: 123 };
          expectedResult = service.addParticipantsToCollectionIfMissing([], null, participants, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(participants);
        });

        it('should return initial array if no Participants is added', () => {
          const participantsCollection: IParticipants[] = [{ id: 123 }];
          expectedResult = service.addParticipantsToCollectionIfMissing(participantsCollection, undefined, null);
          expect(expectedResult).toEqual(participantsCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
