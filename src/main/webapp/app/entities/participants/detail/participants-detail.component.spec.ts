import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ParticipantsDetailComponent } from './participants-detail.component';

describe('Component Tests', () => {
  describe('Participants Management Detail Component', () => {
    let comp: ParticipantsDetailComponent;
    let fixture: ComponentFixture<ParticipantsDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ParticipantsDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ participants: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(ParticipantsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ParticipantsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load participants on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.participants).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
