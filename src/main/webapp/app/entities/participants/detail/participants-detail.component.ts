import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IParticipants } from '../participants.model';

@Component({
  selector: 'emr-participants-detail',
  templateUrl: './participants-detail.component.html',
})
export class ParticipantsDetailComponent implements OnInit {
  participants: IParticipants | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ participants }) => {
      this.participants = participants;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
