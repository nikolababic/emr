import { IUser } from 'app/entities/user/user.model';
import { IConversation } from 'app/entities/conversation/conversation.model';

export interface IParticipants {
  id?: number;
  seen?: boolean | null;
  user?: IUser | null;
  conversation?: IConversation | null;
}

export class Participants implements IParticipants {
  constructor(public id?: number, public seen?: boolean | null, public user?: IUser | null, public conversation?: IConversation | null) {
    this.seen = this.seen ?? false;
  }
}

export function getParticipantsIdentifier(participants: IParticipants): number | undefined {
  return participants.id;
}
