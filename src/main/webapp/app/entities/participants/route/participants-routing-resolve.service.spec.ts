jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IParticipants, Participants } from '../participants.model';
import { ParticipantsService } from '../service/participants.service';

import { ParticipantsRoutingResolveService } from './participants-routing-resolve.service';

describe('Service Tests', () => {
  describe('Participants routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: ParticipantsRoutingResolveService;
    let service: ParticipantsService;
    let resultParticipants: IParticipants | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(ParticipantsRoutingResolveService);
      service = TestBed.inject(ParticipantsService);
      resultParticipants = undefined;
    });

    describe('resolve', () => {
      it('should return IParticipants returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultParticipants = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultParticipants).toEqual({ id: 123 });
      });

      it('should return new IParticipants if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultParticipants = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultParticipants).toEqual(new Participants());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Participants })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultParticipants = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultParticipants).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
