import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IParticipants, Participants } from '../participants.model';
import { ParticipantsService } from '../service/participants.service';

@Injectable({ providedIn: 'root' })
export class ParticipantsRoutingResolveService implements Resolve<IParticipants> {
  constructor(protected service: ParticipantsService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IParticipants> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((participants: HttpResponse<Participants>) => {
          if (participants.body) {
            return of(participants.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Participants());
  }
}
