import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ParticipantsComponent } from '../list/participants.component';
import { ParticipantsDetailComponent } from '../detail/participants-detail.component';
import { ParticipantsUpdateComponent } from '../update/participants-update.component';
import { ParticipantsRoutingResolveService } from './participants-routing-resolve.service';

const participantsRoute: Routes = [
  {
    path: '',
    component: ParticipantsComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ParticipantsDetailComponent,
    resolve: {
      participants: ParticipantsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ParticipantsUpdateComponent,
    resolve: {
      participants: ParticipantsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ParticipantsUpdateComponent,
    resolve: {
      participants: ParticipantsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(participantsRoute)],
  exports: [RouterModule],
})
export class ParticipantsRoutingModule {}
