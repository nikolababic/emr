import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IParticipants } from '../participants.model';
import { ParticipantsService } from '../service/participants.service';

@Component({
  templateUrl: './participants-delete-dialog.component.html',
})
export class ParticipantsDeleteDialogComponent {
  participants?: IParticipants;

  constructor(protected participantsService: ParticipantsService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.participantsService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
