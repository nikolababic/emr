import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ParticipantsComponent } from './list/participants.component';
import { ParticipantsDetailComponent } from './detail/participants-detail.component';
import { ParticipantsUpdateComponent } from './update/participants-update.component';
import { ParticipantsDeleteDialogComponent } from './delete/participants-delete-dialog.component';
import { ParticipantsRoutingModule } from './route/participants-routing.module';

@NgModule({
  imports: [SharedModule, ParticipantsRoutingModule],
  declarations: [ParticipantsComponent, ParticipantsDetailComponent, ParticipantsUpdateComponent, ParticipantsDeleteDialogComponent],
  entryComponents: [ParticipantsDeleteDialogComponent],
})
export class ParticipantsModule {}
