import { Component } from '@angular/core';

@Component({
  selector: 'emr-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['./docs.component.scss'],
})
export class DocsComponent {}
