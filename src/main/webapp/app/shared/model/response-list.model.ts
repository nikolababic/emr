export interface IResponseList {
  status?: boolean;
  list?: Array<any> | null;
  message?: string | null;
}

export class ResponseList implements IResponseList {
  constructor(public status?: boolean, public list?: Array<any> | null, public message?: string | null) {}
}
