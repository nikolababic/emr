import { Pipe, PipeTransform } from '@angular/core';

import * as dayjs from 'dayjs';

@Pipe({
  name: 'formatSqlDate',
})
export class FormatSqlDatePipe implements PipeTransform {
  transform(day: string | null | undefined): string {
    const dayReturn = dayjs(day)
    return dayReturn.format('D MMM YYYY HH:mm');
  }
}
